package com.pimee.mapper;

import com.pimee.model.SysConfig;
import tk.mybatis.mapper.common.Mapper;

public interface SysConfigMapper extends Mapper<SysConfig> {
}