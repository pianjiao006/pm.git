package com.pimee.mapper;

import com.pimee.model.SysTenantRenewLog;
import tk.mybatis.mapper.common.Mapper;

public interface SysTenantRenewLogMapper extends Mapper<SysTenantRenewLog> {
}