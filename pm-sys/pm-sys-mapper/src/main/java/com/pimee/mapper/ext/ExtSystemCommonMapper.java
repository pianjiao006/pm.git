package com.pimee.mapper.ext;

import java.util.List;
import java.util.Map;

public interface ExtSystemCommonMapper {
    /**
     * 分页列表
     *
     * @param params
     * @return
     */
    public List<Map<String, Object>> pageOperLogList(Map<String, Object> params);

    /**
     * 登陆日志分页列表
     * @param param
     * @return
     */
    List<Map<String, Object>> pageLoginInForList(Map<String, Object> param);
}
