package com.pimee.mapper;

import com.pimee.model.SysMenu;
import tk.mybatis.mapper.common.Mapper;

public interface SysMenuMapper extends Mapper<SysMenu> {
}