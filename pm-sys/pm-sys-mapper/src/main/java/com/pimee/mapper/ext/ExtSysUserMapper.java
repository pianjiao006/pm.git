package com.pimee.mapper.ext;

import java.util.List;
import java.util.Map;

import com.pimee.model.SysPost;
import com.pimee.model.SysRole;
import com.pimee.model.SysUserOnline;
import com.pimee.model.vo.SysUserVo;

public interface ExtSysUserMapper {

	/**
	 * 根据条件分页查询用户列表
	 * 
	 * @param params
	 *            用户信息
	 * @return 用户信息集合信息
	 */
	public List<SysUserVo> listUser(Map<String, Object> params);

	/**
	 * 获取用户关联的角色
	 * 
	 * @param userId
	 * @return
	 */
	List<SysRole> selectRolesByUserId(Long userId);
	List<SysRole> selectRolesByUserName(String userName);

	/**
	 * 获取用户关联的岗位
	 * 
	 * @param userId
	 * @return
	 */
	List<SysPost> selectPostsByUserId(Long userId);
	List<SysPost> selectPostsByUserName(String userName);

	/**
	 * 保存在线用户
	 * 
	 * @param param
	 * @return
	 */
	int saveOnline(SysUserOnline param);

	/**
	 * 分页列表
	 * 
	 * @param params
	 * @return
	 */
	public List<Map<String, Object>> pageOnlineList(Map<String, Object> params);

	/**
	 * 通过用户名查询用户
	 * 
	 * @param userName
	 *            用户名
	 * @return 用户对象信息
	 */
	public SysUserVo selectUserByUserName(String userName);
}
