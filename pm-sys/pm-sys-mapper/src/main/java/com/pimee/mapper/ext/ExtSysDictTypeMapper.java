package com.pimee.mapper.ext;

import java.util.List;
import java.util.Map;

import com.pimee.model.SysDictType;

public interface ExtSysDictTypeMapper {

	/**
	 * 分页列表
	 * 
	 * @param params
	 * @return
	 */
	public List<SysDictType> pageList(Map<String, Object> params);
}
