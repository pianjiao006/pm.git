package com.pimee.mapper;

import com.pimee.model.SysUserPost;
import tk.mybatis.mapper.common.Mapper;

public interface SysUserPostMapper extends Mapper<SysUserPost> {
}