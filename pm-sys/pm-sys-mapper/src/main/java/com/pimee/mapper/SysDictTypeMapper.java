package com.pimee.mapper;

import com.pimee.model.SysDictType;
import tk.mybatis.mapper.common.Mapper;

public interface SysDictTypeMapper extends Mapper<SysDictType> {
}