package com.pimee.mapper.ext;

import java.util.List;
import java.util.Map;

/**
 * 租户模块
 */
public interface ExtSysTenantMapper {

    /**
     * 租户分页列表
     *
     * @param param
     * @return
     */
    public List<Map<String, Object>> pageList(Map<String, Object> param);

    /**
     * 租户续费日志分页列表
     * @param param
     * @return
     */
    public List<Map<String, Object>> pageRenewLogList(Map<String, Object> param);
}
