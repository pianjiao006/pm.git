package com.pimee.mapper.ext;

import java.util.List;
import java.util.Map;

import com.pimee.model.SysRole;
import com.pimee.model.SysRoleDept;
import com.pimee.model.SysRoleMenu;

public interface ExtSysRoleMapper {
	/**
	 * 获取用户关联角色
	 * 
	 * @param userId
	 * @return
	 */
	List<SysRole> selectRolesByUserId(Long userId);
	/**
	 * 角色列表（分页）
	 * 
	 * @param params
	 * @return
	 */
	List<SysRole> listRole(Map<String, Object> params);

	/**
	 * 批量删除角色关联代理
	 * @param list
	 */
	void batchRoleDept(List<SysRoleDept> list);

	/**
	 * 批量添加角色菜单
	 * @param list
	 */
	void batchRoleMenu(List<SysRoleMenu> list);
}
