package com.pimee.mapper;

import com.pimee.model.SysTenant;
import tk.mybatis.mapper.common.Mapper;

public interface SysTenantMapper extends Mapper<SysTenant> {
}