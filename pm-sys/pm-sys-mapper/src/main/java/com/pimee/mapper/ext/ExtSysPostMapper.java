package com.pimee.mapper.ext;

import java.util.List;
import java.util.Map;

import com.pimee.model.SysPost;

public interface ExtSysPostMapper {
	/**
	 * 根据用户ID查询岗位
	 * 
	 * @param userId
	 *            用户ID
	 * @return 岗位列表
	 */
	public List<SysPost> selectPostsByUserId(Long userId);

	/**
	 * 分页列表
	 * 
	 * @param params
	 * @return
	 */
	public List<SysPost> pageList(Map<String, Object> params);
	
	/**
     * 根据用户ID获取岗位选择框列表
     * 
     * @param userId 用户ID
     * @return 选中岗位ID列表
     */
    public List<Long> selectPostIdsByUserId(Long userId);
}
