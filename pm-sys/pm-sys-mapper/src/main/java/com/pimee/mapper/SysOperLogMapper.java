package com.pimee.mapper;

import com.pimee.model.SysOperLog;
import tk.mybatis.mapper.common.Mapper;

public interface SysOperLogMapper extends Mapper<SysOperLog> {
}