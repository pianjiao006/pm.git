package com.pimee.mapper;

import com.pimee.model.SysNotice;
import tk.mybatis.mapper.common.Mapper;

public interface SysNoticeMapper extends Mapper<SysNotice> {
}