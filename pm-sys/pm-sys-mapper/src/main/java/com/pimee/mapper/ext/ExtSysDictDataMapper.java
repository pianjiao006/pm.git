package com.pimee.mapper.ext;

import java.util.List;
import java.util.Map;

import com.pimee.model.SysDictData;

public interface ExtSysDictDataMapper {

	/**
	 * 分页列表
	 * 
	 * @param params
	 * @return
	 */
	public List<SysDictData> pageList(Map<String, Object> params);
}
