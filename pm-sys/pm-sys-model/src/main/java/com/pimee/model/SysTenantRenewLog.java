package com.pimee.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

@Table(name = "`sys_tenant_renew_log`")
public class SysTenantRenewLog implements Serializable {
    /**
     * ID
     */
    @Id
    @Column(name = "`id`")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * 租户的ID
     */
    @Column(name = "`tenant_id`")
    private Long tenantId;

    /**
     * 创建时间
     */
    @Column(name = "`create_time`")
    private Date createTime;

    /**
     * 创建人
     */
    @Column(name = "`create_by`")
    private String createBy;

    /**
     * 购买时间
     */
    @Column(name = "`buy_time`")
    private Date buyTime;

    /**
     * 过期时间
     */
    @Column(name = "`expire_time`")
    private Date expireTime;

    /**
     * 续费后过期时间
     */
    @Column(name = "`new_expire_time`")
    private Date newExpireTime;

    /**
     * 续费时长类型: -1|半年，1|1年，2|2年，3|3年
     */
    @Column(name = "`renew_time_type`")
    private Integer renewTimeType;

    @Column(name = "`remark`")
    private String remark;

    private static final long serialVersionUID = 1L;

    /**
     * 获取ID
     *
     * @return id - ID
     */
    public Long getId() {
        return id;
    }

    /**
     * 设置ID
     *
     * @param id ID
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取租户的ID
     *
     * @return tenant_id - 租户的ID
     */
    public Long getTenantId() {
        return tenantId;
    }

    /**
     * 设置租户的ID
     *
     * @param tenantId 租户的ID
     */
    public void setTenantId(Long tenantId) {
        this.tenantId = tenantId;
    }

    /**
     * 获取创建时间
     *
     * @return create_time - 创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建时间
     *
     * @param createTime 创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取创建人
     *
     * @return create_by - 创建人
     */
    public String getCreateBy() {
        return createBy;
    }

    /**
     * 设置创建人
     *
     * @param createBy 创建人
     */
    public void setCreateBy(String createBy) {
        this.createBy = createBy == null ? null : createBy.trim();
    }

    /**
     * 获取购买时间
     *
     * @return buy_time - 购买时间
     */
    public Date getBuyTime() {
        return buyTime;
    }

    /**
     * 设置购买时间
     *
     * @param buyTime 购买时间
     */
    public void setBuyTime(Date buyTime) {
        this.buyTime = buyTime;
    }

    /**
     * 获取过期时间
     *
     * @return expire_time - 过期时间
     */
    public Date getExpireTime() {
        return expireTime;
    }

    /**
     * 设置过期时间
     *
     * @param expireTime 过期时间
     */
    public void setExpireTime(Date expireTime) {
        this.expireTime = expireTime;
    }

    /**
     * 获取续费后过期时间
     *
     * @return new_expire_time - 续费后过期时间
     */
    public Date getNewExpireTime() {
        return newExpireTime;
    }

    /**
     * 设置续费后过期时间
     *
     * @param newExpireTime 续费后过期时间
     */
    public void setNewExpireTime(Date newExpireTime) {
        this.newExpireTime = newExpireTime;
    }

    /**
     * 获取续费时长类型: -1|半年，1|1年，2|2年，3|3年
     *
     * @return renew_time_type - 续费时长类型: -1|半年，1|1年，2|2年，3|3年
     */
    public Integer getRenewTimeType() {
        return renewTimeType;
    }

    /**
     * 设置续费时长类型: -1|半年，1|1年，2|2年，3|3年
     *
     * @param renewTimeType 续费时长类型: -1|半年，1|1年，2|2年，3|3年
     */
    public void setRenewTimeType(Integer renewTimeType) {
        this.renewTimeType = renewTimeType;
    }

    /**
     * @return remark
     */
    public String getRemark() {
        return remark;
    }

    /**
     * @param remark
     */
    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }
}