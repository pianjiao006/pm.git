package com.pimee.model.vo;

import java.util.ArrayList;
import java.util.List;

import com.pimee.model.SysMenu;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class SysMenuVo extends SysMenu {
	 /**
	 * 
	 */
	private static final long serialVersionUID = 801318027181424902L;

	/** 打开方式：menuItem页签 menuBlank新窗口 */
    private String target;

    /** 子菜单 */
    private List<SysMenuVo> children = new ArrayList<>();
    
    private String parentName;
}
