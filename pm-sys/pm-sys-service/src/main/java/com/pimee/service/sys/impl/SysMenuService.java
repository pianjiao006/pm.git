package com.pimee.service.sys.impl;

import java.util.*;
import java.util.stream.Collectors;

import com.alibaba.fastjson.JSONObject;
import com.pimee.common.constants.UserConstants;
import com.pimee.common.utils.security.SecurityUtils;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.google.common.collect.Maps;
import com.pimee.common.exception.BusinessException;
import com.pimee.common.service.impl.BaseService;
import com.pimee.common.utils.DateUtil;
import com.pimee.common.utils.StringUtils;
import com.pimee.mapper.ext.ExtSysMenuMapper;
import com.pimee.model.SysMenu;
import com.pimee.model.SysRole;
import com.pimee.model.SysUser;
import com.pimee.model.vo.MetaVo;
import com.pimee.model.vo.RouterVo;
import com.pimee.model.vo.SysMenuVo;
import com.pimee.model.vo.TreeSelect;
import com.pimee.model.vo.Ztree;
import com.pimee.service.common.TreeUtils;
import com.pimee.service.sys.ISysMenuService;
import com.pimee.service.sys.ISysRoleService;
import com.pimee.service.sys.ISysUserService;

import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;

@Component
public class SysMenuService extends BaseService<SysMenu> implements ISysMenuService {

    @Resource
    private ExtSysMenuMapper extSysMenuMapper;
    @Resource
    private ISysUserService sysUserService;
    @Resource
    private ISysRoleService sysRoleService;

    /**
     * 根据用户查询菜单
     *
     * @param user 用户信息
     * @return 菜单列表
     */
    @Override
    public List<SysMenuVo> selectMenusByUser(SysUser user) {
        List<SysMenuVo> menus = null;
        // 获取当前租户
        Long tenantId = sysUserService.getTenantId(user.getUserId());
        // 超级管理员显示所有菜单信息
        if (sysUserService.isAdmin(user.getUserId())) {
            menus = extSysMenuMapper.selectMenuNormalAll(tenantId);
        } else {
            menus = extSysMenuMapper.selectMenusByUserId(user.getUserId(), tenantId);
        }
        return TreeUtils.getChildPerms(menus, 0);
    }

    /**
     * 根据用户ID查询权限
     *
     * @param userId 用户ID
     * @return 权限列表
     */
    @Override
    public Set<String> selectPermsByUserId(Long userId) {
        List<String> perms = extSysMenuMapper.selectPermsByUserId(userId);
        Set<String> permsSet = new HashSet<>();
        for (String perm : perms) {
            if (StringUtils.isNotEmpty(perm)) {
                permsSet.addAll(Arrays.asList(perm.trim().split(",")));
            }
        }
        return permsSet;
    }

    /**
     * 查询菜单集合
     *
     * @return 所有菜单信息
     */
    @Override
    public List<SysMenuVo> listMenu(Map<String, Object> params) {
        Long userId = SecurityUtils.getUserId();
        params.put("userId", userId);
        if(params.get("tenantId") == null){ // 如果前面没有传入特定的租户 那么就直接获取当前租户
            // 获取当前租户
            Long tenantId = sysUserService.getTenantId(userId);
            params.put("tenantId", tenantId);
        }

        List<SysMenuVo> menuList = null;
        if (sysUserService.isAdmin(userId)) {
            menuList = extSysMenuMapper.selectMenuList(params);
        } else {
            menuList = extSysMenuMapper.selectMenuListByUserId(params);
        }
//        return TreeUtils.getChildPerms(menuList, 0);
        return menuList;
    }

    @Override
    public Object insertMenu(SysMenu param) {
        Example example = new Example(SysMenu.class);
        example.createCriteria().andEqualTo("menuName", param.getMenuName()).andEqualTo("tenantId", param.getTenantId());
        int count = this.countByExample(example);
        if (count > 0) {
            throw new BusinessException("菜单已经存在！");
        }
        Date now = DateUtil.now();
        param.setCreateTime(now);
        param.setUpdateTime(now);
        return this.saveNotNull(param);
    }

    @Override
    public Object deleteMenuById(Long menuId) {
        String msg = "存在子菜单,不允许删除";
        Example example = new Example(SysMenu.class);
        example.createCriteria().andEqualTo("parentId", menuId);
        int count = this.countByExample(example);
        if (count > 0) {
            throw new BusinessException(msg);
        }

        return this.deleteByKey(menuId);
    }

    @Override
    public void updateMenu(SysMenu menu) {
        menu.setUpdateTime(DateUtil.now());
        this.updateNotNull(menu);
    }

    /**
     * 查询所有菜单
     *
     * @return 菜单列表
     */
    @Override
    public List<Ztree> menuTreeData(Long userId) {
        Map<String, Object> params = Maps.newHashMap();
        params.put("userId", userId);
        List<SysMenuVo> menuList = listMenu(params);
        List<Ztree> ztrees = initZtree(menuList);
        return ztrees;
    }

    /**
     * 对象转菜单树
     *
     * @param menuList 菜单列表
     * @return 树结构列表
     */
    public List<Ztree> initZtree(List<SysMenuVo> menuList) {
        return initZtree(menuList, null, false);
    }

    /**
     * 对象转菜单树
     *
     * @param menuList     菜单列表
     * @param roleMenuList 角色已存在菜单列表
     * @param permsFlag    是否需要显示权限标识
     * @return 树结构列表
     */
    public List<Ztree> initZtree(List<SysMenuVo> menuList, List<String> roleMenuList, boolean permsFlag) {
        List<Ztree> ztrees = new ArrayList<>();
        boolean isCheck = StringUtils.isNotNull(roleMenuList);
        for (SysMenuVo menu : menuList) {
            Ztree ztree = new Ztree();
            ztree.setId(menu.getMenuId());
            ztree.setpId(menu.getParentId());
            ztree.setName(transMenuName(menu, permsFlag));
            ztree.setTitle(menu.getMenuName());
            if (isCheck) {
                ztree.setChecked(roleMenuList.contains(menu.getMenuId() + menu.getPerms()));
            }
            ztrees.add(ztree);
        }
        return ztrees;
    }

    public String transMenuName(SysMenuVo menu, boolean permsFlag) {
        StringBuffer sb = new StringBuffer();
        sb.append(menu.getMenuName());
        if (permsFlag) {
            sb.append("<font color=\"#888\">&nbsp;&nbsp;&nbsp;" + menu.getPerms() + "</font>");
        }
        return sb.toString();
    }

    @Override
    public List<Ztree> roleMenuTreeData(Long userId, Long roleId) {
        Map<String, Object> params = Maps.newHashMap();
        params.put("userId", userId);
        List<SysMenuVo> menuList = listMenu(params);
        List<Ztree> ztrees = new ArrayList<Ztree>();
        if (StringUtils.isNotNull(roleId)) {
            List<String> roleMenuList = extSysMenuMapper.selectMenuTree(roleId);
            ztrees = initZtree(menuList, roleMenuList, true);
        } else {
            ztrees = initZtree(menuList, null, true);
        }
        return ztrees;
    }

    /**
     * 构建前端路由所需要的菜单
     *
     * @param menus 菜单列表
     * @return 路由列表
     */
    @Override
    public List<RouterVo> buildMenus(List<SysMenuVo> menus) {
        List<RouterVo> routers = new LinkedList<RouterVo>();
        for (SysMenuVo menu : menus) {
            RouterVo router = new RouterVo();
            router.setHidden("1".equals(menu.getVisible()));
            router.setName(getRouteName(menu));
            router.setPath(getRouterPath(menu));
            router.setComponent(getComponent(menu));
            router.setMeta(new MetaVo(menu.getMenuName(), menu.getIcon(), StringUtils.equals("1", menu.getIsCache()+"")));
            List<SysMenuVo> cMenus = menu.getChildren();
            if (!cMenus.isEmpty() && cMenus.size() > 0 && UserConstants.TYPE_DIR.equals(menu.getMenuType())) {
                router.setAlwaysShow(true);
                router.setRedirect("noRedirect");
                router.setChildren(buildMenus(cMenus));
            } else if (isMenuFrame(menu)) {
                router.setMeta(null);
                List<RouterVo> childrenList = new ArrayList<RouterVo>();
                RouterVo children = new RouterVo();
                children.setPath(menu.getPath());
                children.setComponent(menu.getComponent());
                children.setName(StringUtils.capitalize(menu.getPath()));
                children.setMeta(new MetaVo(menu.getMenuName(), menu.getIcon(), StringUtils.equals("1", menu.getIsCache()+"")));
                childrenList.add(children);
                router.setChildren(childrenList);
            }
            routers.add(router);
        }
        return routers;
    }

    /**
     * 获取路由名称
     *
     * @param menu 菜单信息
     * @return 路由名称
     */
    public String getRouteName(SysMenu menu) {
        String routerName = StringUtils.capitalize(menu.getPath());
        // 非外链并且是一级目录（类型为目录）
        if (isMenuFrame(menu)) {
            routerName = StringUtils.EMPTY;
        }
        return routerName;
    }

    /**
     * 获取组件信息
     *
     * @param menu 菜单信息
     * @return 组件信息
     */
    public String getComponent(SysMenu menu) {
        String component = UserConstants.LAYOUT;
        if (StringUtils.isNotEmpty(menu.getComponent()) && !isMenuFrame(menu)) {
            component = menu.getComponent();
        } else if (StringUtils.isEmpty(menu.getComponent()) && isParentView(menu)) {
            component = UserConstants.PARENT_VIEW;
        }
        return component;
    }

    /**
     * 是否为菜单内部跳转
     *
     * @param menu 菜单信息
     * @return 结果
     */
    public boolean isMenuFrame(SysMenu menu) {
        return menu.getParentId().intValue() == 0 && UserConstants.TYPE_MENU.equals(menu.getMenuType())
                && menu.getIsFrame().equals(UserConstants.NO_FRAME);
    }

    /**
     * 是否为parent_view组件
     *
     * @param menu 菜单信息
     * @return 结果
     */
    public boolean isParentView(SysMenu menu) {
        return menu.getParentId().intValue() != 0 && UserConstants.TYPE_DIR.equals(menu.getMenuType());
    }

    /**
     * 根据角色ID查询菜单树信息
     *
     * @param roleId 角色ID
     * @return 选中菜单列表
     */
    public List<Integer> selectMenuListByRoleId(Long roleId) {
        SysRole sysRole = sysRoleService.selectByKey(roleId);
        return extSysMenuMapper.selectMenuListByRoleId(roleId, sysRole.isMenuCheckStrictly());
    }

    /**
     * 获取路由地址
     *
     * @param menu 菜单信息
     * @return 路由地址
     */
    public String getRouterPath(SysMenu menu) {
        String routerPath = menu.getPath();
        // 非外链并且是一级目录
        if (0 == menu.getParentId() && "1".equals(menu.getIsFrame() + "")) {
            routerPath = "/" + menu.getPath();
        }
        return routerPath;
    }

    /**
     * 构建前端所需要下拉树结构
     *
     * @param menus 菜单列表
     * @return 下拉树结构列表
     */
    @Override
    public List<TreeSelect> buildMenuTreeSelect(List<SysMenuVo> menus) {
        List<SysMenuVo> menuTrees = buildMenuTree(menus);
        return menuTrees.stream().map(TreeSelect::new).collect(Collectors.toList());
    }

	public List<SysMenuVo> buildMenuTree(List<SysMenuVo> menus) {
		List<SysMenuVo> returnList = new ArrayList<>();
		List<Long> tempList = new ArrayList<Long>();
		for (SysMenu dept : menus) {
			tempList.add(dept.getMenuId());
		}
		for (Iterator<SysMenuVo> iterator = menus.iterator(); iterator.hasNext();) {
			SysMenuVo menu = iterator.next();
			// 如果是顶级节点, 遍历该父节点的所有子节点
			if (!tempList.contains(menu.getParentId())) {
				recursionFn(menus, menu);
				returnList.add(menu);
			}
		}
		if (returnList.isEmpty()) {
			returnList = menus;
		}
		return returnList;
	}

	/**
	 * 递归列表
	 *
	 * @param list
	 * @param t
	 */
	private void recursionFn(List<SysMenuVo> list, SysMenuVo t) {
		// 得到子节点列表
		List<SysMenuVo> childList = getChildList(list, t);
		t.setChildren(childList);
		for (SysMenuVo tChild : childList) {
			if (hasChild(list, tChild)) {
				recursionFn(list, tChild);
			}
		}
	}

	/**
	 * 得到子节点列表
	 */
	private List<SysMenuVo> getChildList(List<SysMenuVo> list, SysMenu t) {
		List<SysMenuVo> tlist = new ArrayList<>();
		Iterator<SysMenuVo> it = list.iterator();
		while (it.hasNext()) {
			SysMenuVo n = it.next();
			if (n.getParentId().longValue() == t.getMenuId().longValue()) {
				tlist.add(n);
			}
		}
		return tlist;
	}

	/**
	 * 判断是否有子节点
	 */
	private boolean hasChild(List<SysMenuVo> list, SysMenuVo t) {
		return getChildList(list, t).size() > 0 ? true : false;
	}


    /**
     * 初始化菜单数据
     * @param fromTenantId
     * @param toTenantId
     */
    @Override
    public void copyMenuFrom(Long fromTenantId, Long toTenantId) {
        Example example = new Example(SysMenu.class);
        example.createCriteria().andEqualTo("tenantId", fromTenantId);
        List<SysMenu> menuList = this.selectByExample(example);
        Date now = DateUtil.now();
        String userName = SecurityUtils.getUsername();
        for(SysMenu sysMenu : menuList){
            if(0 == sysMenu.getParentId()){ // 第一层
                Long parentMenuId = sysMenu.getMenuId();// 旧的父级ID
                sysMenu.setMenuId(null);
                sysMenu.setCreateTime(now);
                sysMenu.setCreateBy(userName);
                sysMenu.setUpdateTime(now);
                sysMenu.setUpdateBy(userName);
                sysMenu.setTenantId(toTenantId);
                this.insertMenu(sysMenu);

                // 递归插入菜单
                this.recuiMenu(menuList, sysMenu, parentMenuId, now, userName, toTenantId);
            }
        }
    }

    /**
     * 递归查找子级并插入
     * @param menuList
     * @param sysMenuParent
     * @param now
     * @param userName
     * @param toTenantId
     */
    private void recuiMenu(List<SysMenu> menuList, SysMenu sysMenuParent, Long parentMenuId, Date now, String userName, Long toTenantId){
        for (SysMenu sysMenu : menuList){
            if(sysMenu.getParentId().longValue() == parentMenuId.longValue()){
                Long pmenuId = sysMenu.getMenuId(); //用于下一轮的菜单父级ID对比
                sysMenu.setMenuId(null);
                sysMenu.setParentId(sysMenuParent.getMenuId());
                sysMenu.setCreateTime(now);
                sysMenu.setCreateBy(userName);
                sysMenu.setUpdateTime(now);
                sysMenu.setUpdateBy(userName);
                sysMenu.setTenantId(toTenantId);
                this.insertMenu(sysMenu);
                // 递归
                this.recuiMenu(menuList, sysMenu, pmenuId, now, userName, toTenantId);
            }
        }
    }

    /**
     * 递归测试
     * @param args
     */
    public static void main(String[] args) {
        List<SysMenu> list = init();
        for (SysMenu sysMenu : list){
            if(sysMenu.getParentId() == 0){
                System.out.println(JSONObject.toJSONString(sysMenu));
                re(list, sysMenu);
            }
        }
    }
    private static void re(List<SysMenu> list, SysMenu pmenu){
        for (SysMenu sysMenu : list){
            if(sysMenu.getParentId() == pmenu.getMenuId()){
                System.out.println(JSONObject.toJSONString(sysMenu));
                re(list, sysMenu);
            }
        }
    }
    private static List<SysMenu> init(){
        List<SysMenu> list = new ArrayList<>();
        SysMenu menu = new SysMenu();
        menu.setMenuId(1l);
        menu.setParentId(0l);
        menu.setMenuName("第0级");
        list.add(menu);

        SysMenu menu0 = new SysMenu();
        menu0.setMenuId(5l);
        menu0.setParentId(0l);
        menu0.setMenuName("第0级");
        list.add(menu0);

        SysMenu menu1 = new SysMenu();
        menu1.setMenuId(2l);
        menu1.setParentId(1l);
        menu1.setMenuName("第1级");
        list.add(menu1);

        SysMenu menu2 = new SysMenu();
        menu2.setMenuId(3l);
        menu2.setParentId(2l);
        menu2.setMenuName("第2级");
        list.add(menu2);

        SysMenu menu3 = new SysMenu();
        menu3.setMenuId(4l);
        menu3.setParentId(2l);
        menu3.setMenuName("第2级");
        list.add(menu3);

        return list;
    }

}
