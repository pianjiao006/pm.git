package com.pimee.service.sys;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.pimee.common.service.IBaseService;
import com.pimee.model.SysMenu;
import com.pimee.model.SysUser;
import com.pimee.model.vo.RouterVo;
import com.pimee.model.vo.SysMenuVo;
import com.pimee.model.vo.TreeSelect;
import com.pimee.model.vo.Ztree;

public interface ISysMenuService extends IBaseService<SysMenu> {

	/**
	 * 获取用户菜单
	 * 
	 * @param user
	 * @return
	 */
	List<SysMenuVo> selectMenusByUser(SysUser user);

	/**
	 * 获取用户权限
	 * 
	 * @param userId
	 * @return
	 */
	Set<String> selectPermsByUserId(Long userId);

	/**
	 * 查询菜单
	 * 
	 * @param params
	 * @return
	 */
	List<SysMenuVo> listMenu(Map<String, Object> params);

	/**
	 * 新增菜单
	 * 
	 * @param menu
	 * @return
	 */
	Object insertMenu(SysMenu menu);

	/**
	 * 删除菜单
	 * 
	 * @param menuId
	 * @return
	 */
	Object deleteMenuById(Long menuId);

	/**
	 * 更新订单信息
	 * 
	 * @param menu
	 */
	void updateMenu(SysMenu menu);

	/**
	 * 获取用户相关菜单
	 * 
	 * @param userId
	 * @return
	 */
	List<Ztree> menuTreeData(Long userId);

	/**
	 * 获取角色菜单树
	 * 
	 * @param roleId
	 * @return
	 */
	List<Ztree> roleMenuTreeData(Long userId, Long roleId);

	/**
	 * 构建前端路由所需要的菜单
	 * 
	 * @param menus
	 * @return
	 */
//	List<RouterVo> buildMenus(List<SysMenuVo> menus);
	public List<RouterVo> buildMenus(List<SysMenuVo> menus);

	/**
	 * 获取角色关联菜单列表
	 * 
	 * @param roleId
	 * @return
	 */
	List<Integer> selectMenuListByRoleId(Long roleId);

	/**
	 * 构建前端所需要下拉树结构
	 * 
	 * @param menus
	 * @return
	 */
	List<TreeSelect> buildMenuTreeSelect(List<SysMenuVo> menus);

	/**
	 * 复制一份菜单
	 * @param fromTenantId
	 * @param toTenantId
	 */
    void copyMenuFrom(Long fromTenantId, Long toTenantId);
}
