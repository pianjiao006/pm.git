package com.pimee.service.sys;

import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;

public interface ISysFileService {

    /**
     * 上传图片文件
     *
     * @param file
     */
    public String uploadFile(MultipartFile file);

    /**
     * 校验文件是否存在
     *
     * @param fileName
     * @return
     */
    public boolean checkExist(String fileName);

    /**
     * 下载文件
     *
     * @param fileName
     * @return
     */
    public InputStream downloadFile(String fileName);

    /**
     * 删除文件
     *
     * @param fileName
     */
    void removeFile(String fileName);
}
