package com.pimee.service.sys;

import java.util.List;
import java.util.Map;

import com.github.pagehelper.PageInfo;
import com.pimee.common.service.IBaseService;
import com.pimee.model.SysDictType;
import com.pimee.model.vo.Ztree;

public interface ISysDictTypeService extends IBaseService<SysDictType> {
	/**
	 * 分页列表
	 * 
	 * @param params
	 * @return
	 */
	PageInfo<SysDictType> pageList(Map<String, Object> params);

	/**
	 * 新增数据字典类型
	 * 
	 * @param param
	 */
	void insertDictType(SysDictType param);

	/**
	 * 更新数据字典类型
	 * 
	 * @param param
	 */
	void updateDictType(SysDictType param);

	/**
	 * 根据类型编码获取类型数据
	 * 
	 * @param dictType
	 * @return
	 */
	SysDictType selectDictTypeByType(String dictType);

	/**
	 * 获取字典类型树
	 * 
	 * @return
	 */
	List<Ztree> selectDictTree();

	/**
	 * 删除多条记录
	 * @param dictIds
	 */
	void deleteByIds(Long[] dictIds);

	/**
	 * 清理缓存
	 */
	public void clearCache();
}
