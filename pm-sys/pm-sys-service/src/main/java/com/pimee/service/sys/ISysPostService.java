package com.pimee.service.sys;

import java.util.List;
import java.util.Map;

import com.github.pagehelper.PageInfo;
import com.pimee.common.service.IBaseService;
import com.pimee.model.SysPost;

public interface ISysPostService extends IBaseService<SysPost> {
	/**
	 * 获取用户岗位
	 * 
	 * @param userId
	 * @return
	 */
	List<SysPost> selectPostsByUserId(Long userId);

	/**
	 * 分页列表
	 * 
	 * @param params
	 * @return
	 */
	PageInfo<SysPost> pageList(Map<String, Object> params);

	/**
	 * 新增岗位
	 * 
	 * @param param
	 */
	void insertPost(SysPost param);

	/**
	 * 更新岗位
	 * 
	 * @param param
	 */
	void updatePost(SysPost param);

	/**
	 * 获取岗位Id列表
	 * 
	 * @param userId
	 * @return
	 */
	List<Long> selectPostIdsByUserId(Long userId);

	/**
	 * 删除多条记录
	 * @param postIds
	 */
	void deleteByIds(Long[] postIds);
}
