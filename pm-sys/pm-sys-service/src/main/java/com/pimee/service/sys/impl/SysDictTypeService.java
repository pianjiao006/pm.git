package com.pimee.service.sys.impl;

import java.util.*;

import com.pimee.common.core.support.HttpCode;
import com.pimee.common.exception.BusinessException;
import com.pimee.common.service.impl.BaseService;
import com.pimee.service.common.DictUtils;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageInfo;
import com.pimee.common.utils.DateUtil;
import com.pimee.mapper.ext.ExtSysDictTypeMapper;
import com.pimee.model.SysDictType;
import com.pimee.model.vo.Ztree;
import com.pimee.service.sys.ISysDictTypeService;

import lombok.extern.slf4j.Slf4j;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;

@Slf4j
@Service
public class SysDictTypeService extends BaseService<SysDictType> implements ISysDictTypeService {

    @Resource
    private ExtSysDictTypeMapper extSysDictTypeMapper;

    @Override
    public PageInfo<SysDictType> pageList(Map<String, Object> params) {
        log.info("===> 字典类型列表分页查询...");
        startPage(params);// 设置分页
        List<SysDictType> list = extSysDictTypeMapper.pageList(params);
        return new PageInfo<>(list);
    }

    @Override
    public void insertDictType(SysDictType param) {
        Date now = DateUtil.now();
        param.setCreateTime(now);
        param.setUpdateTime(now);
        this.saveNotNull(param);
    }

    @Override
    public void updateDictType(SysDictType param) {
        param.setUpdateTime(DateUtil.now());
        this.updateNotNull(param);
    }

    @Override
    public SysDictType selectDictTypeByType(String dictType) {
        Example example = new Example(SysDictType.class);
        example.createCriteria().andEqualTo("dictType", dictType);
        return this.selectOneByExample(example);
    }

    /**
     * 查询字典类型树
     *
     * @return 所有字典类型
     */
    @Override
    public List<Ztree> selectDictTree() {
        List<Ztree> ztrees = new ArrayList<>();
        List<SysDictType> dictList = this.listAll();
        for (SysDictType dict : dictList) {
            if ("1".equals(dict.getStatus())) {
                Ztree ztree = new Ztree();
                ztree.setId(dict.getDictId());
                ztree.setName(transDictName(dict));
                ztree.setTitle(dict.getDictType());
                ztrees.add(ztree);
            }
        }
        return ztrees;
    }

    public String transDictName(SysDictType dictType) {
        StringBuffer sb = new StringBuffer();
        sb.append("(" + dictType.getDictName() + ")");
        sb.append("&nbsp;&nbsp;&nbsp;" + dictType.getDictType());
        return sb.toString();
    }

    @Override
    public void deleteByIds(Long[] dictIds) {
        if (dictIds == null || dictIds.length == 0) {
            throw new BusinessException(HttpCode.INTERNAL_SERVER_ERROR.value(), "没有传入任何要删除的标识");
        }

        for (Long dictId : dictIds) {
            SysDictType dictType = this.selectByKey(dictId);
            if (dictType != null) {
                Example example = new Example(SysDictType.class);
                example.createCriteria().andEqualTo("dictType", dictType);
                int dictTypeCount = this.countByExample(example);
                if (dictTypeCount > 0) {
                    throw new BusinessException(HttpCode.INTERNAL_SERVER_ERROR.value(), String.format("%1$s已分配,不能删除", dictType.getDictName()));
                }
            }
        }

        Example example = new Example(SysDictType.class);
        List<Long> dictIdList = Arrays.asList(dictIds);
        example.createCriteria().andIn("dictId", dictIdList);
        this.deleteBy(example);
    }

    /**
     * 清空缓存数据
     */
    @Override
    public void clearCache() {
        DictUtils.clearDictCache();
    }
}
