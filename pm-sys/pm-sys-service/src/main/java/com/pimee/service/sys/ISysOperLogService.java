package com.pimee.service.sys;

import com.github.pagehelper.PageInfo;
import com.pimee.common.service.IBaseService;
import com.pimee.model.SysOperLog;

import java.util.Map;

/**
 * 操作日志 服务层
 *
 * @author ruoyi
 */
public interface ISysOperLogService extends IBaseService<SysOperLog> {
    /**
     * 查询系统操作日志集合
     *
     * @param param 操作日志对象
     * @return 操作日志集合
     */
    public PageInfo<Map<String, Object>> pageList(Map<String, Object> param);
    /**
     * 新增操作日志
     *
     * @param param 操作日志对象
     */
    public void insert(SysOperLog param);

    /**
     * 批量删除系统操作日志
     *
     * @param operIds 需要删除的操作日志ID
     * @return 结果
     */
    public void deleteByIds(Long[] operIds);

    /**
     * 清空操作日志
     */
    public void cleanOperLog();
}
