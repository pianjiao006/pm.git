package com.pimee.service.sys.impl;

import com.github.pagehelper.PageInfo;
import com.google.common.collect.Lists;
import com.pimee.common.core.support.HttpCode;
import com.pimee.common.exception.BusinessException;
import com.pimee.common.service.impl.BaseService;
import com.pimee.common.utils.DateUtil;
import com.pimee.common.utils.bean.BeanUtils;
import com.pimee.mapper.ext.ExtSysDictDataMapper;
import com.pimee.model.SysDictData;
import com.pimee.model.vo.SysDictDataVo;
import com.pimee.service.sys.ISysDictDataService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
public class SysDictDataService extends BaseService<SysDictData> implements ISysDictDataService {

	@Resource
	private ExtSysDictDataMapper extSysDictDataMapper;

	@Override
	public PageInfo<SysDictData> pageList(Map<String, Object> params) {
		log.info("===> 字典列表分页查询...");
		startPage(params);// 设置分页
		List<SysDictData> list = extSysDictDataMapper.pageList(params);
		return new PageInfo<>(list);
	}

	@Override
	public SysDictData selectDictDataById(Long dictCode) {
		SysDictData dictData = new SysDictData();
		dictData.setDictCode(dictCode);
		dictData = this.selectOne(dictData);
		return dictData;
	}

	/**
	 * 根据字典类型查询字典数据
	 * 
	 * @param dictType
	 *            字典类型
	 * @return 字典数据集合信息
	 */
	@Override
	public List<SysDictDataVo> selectDictDataByType(String dictType) {
		Example example = new Example(SysDictData.class);
		example.createCriteria().andEqualTo("dictType", dictType).andEqualTo("status", 0);
		example.orderBy("dictSort").asc();
		List<SysDictData> dictDataList = this.selectByExample(example);
		List<SysDictDataVo> dictDataVoList = Lists.newArrayList();
		for (SysDictData dictData : dictDataList) {
			SysDictDataVo dictDataVo = new SysDictDataVo();
			BeanUtils.copyBeanProp(dictDataVo, dictData);
			dictDataVo.setOk("Y".equals(dictData.getIsDefault()));
			dictDataVoList.add(dictDataVo);
		}
		return dictDataVoList;
	}

	/**
	 * 根据字典类型和字典键值查询字典数据信息
	 * 
	 * @param dictType
	 *            字典类型
	 * @param dictValue
	 *            字典键值
	 * @return 字典标签
	 */
	@Override
	public String selectDictLabel(String dictType, String dictValue) {
		SysDictData dictData = new SysDictData();
		dictData.setDictType(dictType);
		dictData.setDictValue(dictValue);
		dictData = this.selectOne(dictData);
		if (dictData == null) {
			return "";
		}
		return dictData.getDictLabel();
	}

	@Override
	public void insertDictData(SysDictData param) {
		Date now = DateUtil.now();
		param.setCreateTime(now);
		param.setUpdateTime(now);
		this.saveNotNull(param);
	}

	@Override
	public void updateDictData(SysDictData param) {
		param.setUpdateTime(DateUtil.now());
		this.updateNotNull(param);
	}

	@Override
	public void deleteByIds(Long[] dictCodes) {
		if (dictCodes == null || dictCodes.length == 0) {
			throw new BusinessException(HttpCode.INTERNAL_SERVER_ERROR.value(), "没有传入任何要删除的标识");
		}
		Example example = new Example(SysDictData.class);
		List<Long> dictCodeList = Arrays.asList(dictCodes);
		example.createCriteria().andIn("dictCode", dictCodeList);
		this.deleteBy(example);
	}
}
