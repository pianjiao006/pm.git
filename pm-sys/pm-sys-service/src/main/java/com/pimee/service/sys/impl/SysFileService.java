package com.pimee.service.sys.impl;

import com.pimee.oss.AliyunStorageService;
import com.pimee.service.sys.ISysFileService;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.IOException;
import java.io.InputStream;

@Log4j2
@Service
public class SysFileService implements ISysFileService {
    @Resource
    private AliyunStorageService aliyunStorageService;

    @Override
    public String uploadFile(MultipartFile file) {
        try {
            String oriFileName = file.getOriginalFilename();
            InputStream inputStream = file.getInputStream();
            long contentLength = file.getSize();
            String contentType = file.getContentType();
            String fileName = aliyunStorageService.upload(oriFileName, inputStream, contentLength, contentType);
            log.info("===> fileName: {}", fileName);
            return fileName;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean checkExist(String fileName) {
        return aliyunStorageService.checkExist(fileName);
    }

    @Override
    public InputStream downloadFile(String fileName){
        return aliyunStorageService.download(fileName);
    }

    @Override
    public void removeFile(String fileName) {
        aliyunStorageService.removeFile(fileName);
    }
}
