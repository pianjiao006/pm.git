package com.pimee.service.sys;

import java.util.List;
import java.util.Map;

import com.github.pagehelper.PageInfo;
import com.pimee.common.service.IBaseService;
import com.pimee.model.SysDictData;
import com.pimee.model.vo.SysDictDataVo;

public interface ISysDictDataService extends IBaseService<SysDictData> {
	/**
	 * 通过数据类型过去字典数据
	 * 
	 * @param dictType
	 * @return
	 */
	List<SysDictDataVo> selectDictDataByType(String dictType);

	/**
	 * 获取字典数据
	 * 
	 * @param dictCode
	 * @return
	 */
	SysDictData selectDictDataById(Long dictCode);

	/**
	 * 获取字典标签
	 * 
	 * @param dictType
	 * @param dictValue
	 * @return
	 */
	String selectDictLabel(String dictType, String dictValue);

	/**
	 * 分页列表
	 * 
	 * @param params
	 * @return
	 */
	PageInfo<SysDictData> pageList(Map<String, Object> params);

	/**
	 * 新增字典
	 * 
	 * @param param
	 */
	void insertDictData(SysDictData param);

	/**
	 * 更新字典
	 * 
	 * @param param
	 */
	void updateDictData(SysDictData param);

	/**
	 * 删除字典
	 * @param dictIds
	 */
	void deleteByIds(Long[] dictIds);
}
