package com.pimee.service.sys;

import java.util.Map;

import com.github.pagehelper.PageInfo;
import com.pimee.common.service.IBaseService;
import com.pimee.model.SysNotice;

public interface ISysNoticeService extends IBaseService<SysNotice> {
	/**
	 * 新增通知
	 * 
	 * @param param
	 */
	void insertNotice(SysNotice param);

	/**
	 * 更新通知
	 * 
	 * @param param
	 */
	void updateNotice(SysNotice param);

	/**
	 * 分页列表
	 * 
	 * @param params
	 * @return
	 */
	PageInfo<SysNotice> pageList(Map<String, Object> params);

	/**
	 * 删除
	 * @param noticeIds
	 */
    void deleteByIds(Long[] noticeIds);
}
