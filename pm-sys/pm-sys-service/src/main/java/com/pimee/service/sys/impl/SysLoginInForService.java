package com.pimee.service.sys.impl;

import com.pimee.common.service.impl.BaseService;
import com.github.pagehelper.PageInfo;
import com.pimee.mapper.SysLoginInForMapper;
import com.pimee.mapper.common.PublicMapper;
import com.pimee.mapper.ext.ExtSystemCommonMapper;
import com.pimee.model.SysLoginInFor;
import com.pimee.service.sys.ISysLoginInForService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * 系统访问日志情况信息 服务层处理
 * 
 * @author Bruce Shaw 2020年4月13日 下午4:47:28
 */
@Slf4j
@Service
public class SysLoginInForService extends BaseService<SysLoginInFor> implements ISysLoginInForService {

	@Resource
	private ExtSystemCommonMapper extSystemCommonMapper;
	@Resource
	private SysLoginInForMapper sysLoginInForMapper;
	@Resource
	private PublicMapper publicMapper;

	@Override
	public PageInfo<Map<String, Object>> pageList(Map<String, Object> param) {
		log.info("===> 租户列表分页查询...");
		startPage(param);// 设置分页
		List<Map<String, Object>> list = extSystemCommonMapper.pageLoginInForList(param);
		return new PageInfo<>(list);
	}

	/**
	 * 新增系统登录日志
	 * 
	 * @param logininfor
	 *            访问日志对象
	 */
	@Override
	public void insertLoginInFor(SysLoginInFor logininfor) {
		sysLoginInForMapper.insertSelective(logininfor);
	}

	/**
	 * 查询系统登录日志集合
	 * 
	 * @param logininfor
	 *            访问日志对象
	 * @return 登录记录集合
	 */
	@Override
	public List<SysLoginInFor> selectLoginInForList(SysLoginInFor logininfor) {
		return sysLoginInForMapper.select(logininfor);
	}

	/**
	 * 批量删除系统登录日志
	 * 
	 * @param infoIds
	 *            需要删除的登录日志ID
	 * @return
	 */
	@Override
	public int deleteByIds(Long[] infoIds) {
		Example example = new Example(SysLoginInFor.class);
		example.createCriteria().andIn("infoId", Arrays.asList(infoIds));
		return sysLoginInForMapper.deleteByExample(example);
	}

	/**
	 * 清空系统登录日志
	 */
	@Override
	public void cleanLoginInFor() {
		publicMapper.executeSql("truncate table `sys_login_in_for`");
	}
}
