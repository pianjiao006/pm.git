package com.pimee.service.sys;

import java.util.Map;

import com.github.pagehelper.PageInfo;
import com.pimee.common.service.IBaseService;
import com.pimee.model.SysConfig;

public interface ISysConfigService extends IBaseService<SysConfig> {

	/**
	 * 直接从数据库通过key获取
	 * 
	 * @param configKey
	 * @return
	 */
	String selectConfigByKeyForce(String configKey);
	
	/**
	 * 新增配置
	 * 
	 * @param param
	 */
	void insertConfig(SysConfig param);

	/**
	 * 更新配置
	 * 
	 * @param param
	 */
	void updateConfig(SysConfig param);

	/**
	 * 分页列表
	 * 
	 * @param params
	 * @return
	 */
	PageInfo<SysConfig> pageList(Map<String, Object> params);

	/**
	 * 根据key获取val
	 * 
	 * @param configKey
	 * @param defaultValue
	 * @return
	 */
	String getConfigValue(String configKey, String defaultValue);

	/**
	 * 获取配置值
	 * 
	 * @param configKey
	 * @return
	 */
	String getConfigValue(String configKey);

	/**
	 * 删除参数配置
	 * @param configIds
	 */
	void deleteByIds(Long[] configIds);

	/**
	 * 清理缓存
	 */
	void clearCache();
}
