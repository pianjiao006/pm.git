package com.pimee.service.sys.impl;

import com.github.pagehelper.PageInfo;
import com.pimee.common.core.support.HttpCode;
import com.pimee.common.exception.BusinessException;
import com.pimee.common.service.impl.BaseService;
import com.pimee.common.utils.DateUtil;
import com.pimee.mapper.ext.ExtSysTenantMapper;
import com.pimee.model.SysTenantRenewLog;
import com.pimee.service.sys.ISysTenantRenewLogService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
public class SysTenantRenewLogService extends BaseService<SysTenantRenewLog> implements ISysTenantRenewLogService {
    @Resource
    private ExtSysTenantMapper extSysTenantMapper;

    @Override
    public PageInfo<Map<String, Object>> pageList(Map<String, Object> param) {
        log.info("===> 租户续费日志列表分页查询...");
        startPage(param);// 设置分页
        List<Map<String, Object>> list = extSysTenantMapper.pageRenewLogList(param);
        return new PageInfo<>(list);
    }

    @Override
    public void insert(SysTenantRenewLog param) {
        Date now = DateUtil.now();
        param.setCreateTime(now);
        this.saveNotNull(param);
    }

    @Override
    public void deleteByIds(Long[] ids) {
        if (ids == null || ids.length == 0) {
            throw new BusinessException(HttpCode.INTERNAL_SERVER_ERROR.value(), "没有传入任何要删除的标识");
        }
        Example example = new Example(SysTenantRenewLog.class);
        List<Long> idList = Arrays.asList(ids);
        example.createCriteria().andIn("id", idList);
        this.deleteBy(example);
    }
}
