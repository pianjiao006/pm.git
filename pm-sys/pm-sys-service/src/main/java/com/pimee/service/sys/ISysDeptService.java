package com.pimee.service.sys;

import java.util.List;
import java.util.Map;

import com.pimee.common.service.IBaseService;
import com.pimee.model.SysDept;
import com.pimee.model.vo.TreeSelect;

public interface ISysDeptService extends IBaseService<SysDept> {
	/**
	 * 根据deptId获取代理信息
	 * 
	 * @param deptId
	 * @return
	 */
	SysDept selectDeptById(Long deptId);

	/**
	 * 获取代理列表
	 * 
	 * @param params
	 * @return
	 */
	List<SysDept> listDept(Map<String, Object> params);

	/**
	 * 新增代理
	 * 
	 * @param param
	 */
	void insertDept(SysDept param);

	/**
	 * 更新代理
	 * 
	 * @param param
	 */
	void updateDept(SysDept param);

	/**
	 * 根据代理标识删除代理信息
	 * 
	 * @param deptId
	 */
	void deleteDeptById(Long deptId);

	/**
	 * 通过代理树来构建
	 * 
	 * @param depts
	 * @return
	 */
	List<SysDept> buildDeptTree(List<SysDept> depts);

	/**
	 * 构建代理树
	 * 
	 * @return
	 */
	List<TreeSelect> buildDeptTreeSelect();

	/**
	 * 通过角色获取相关代理信息
	 * @param roleId
	 * @return
	 */
	public List<Integer> selectDeptListByRoleId(Long roleId);

	/**
	 * 校验部门名称是否唯一
	 *
	 * @param dept 部门信息
	 * @return 结果
	 */
	public String checkDeptNameUnique(SysDept dept);

}
