package com.pimee.support.security.handler;

import java.io.IOException;
import java.io.Serializable;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSON;
import com.google.common.collect.Maps;
import com.pimee.common.utils.ServletUtils;
import com.pimee.common.utils.StringUtils;

/**
 * 认证失败处理类 返回未授权O
 * 
 * @author ruoyi
 */
@Component
public class AuthenticationEntryPointImpl implements AuthenticationEntryPoint, Serializable {
	private static final long serialVersionUID = -8970718410437077606L;

	@Override
	public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException e)
			throws IOException {
		int code = HttpStatus.SC_UNAUTHORIZED;
		String msg = StringUtils.format("请求访问：{}，认证失败，无法访问系统资源", request.getRequestURI());
		Map<String, Object> result = Maps.newHashMap();
		result.put("code", code);
		result.put("msg", msg);
		ServletUtils.renderString(response, JSON.toJSONString(result));
	}
}
