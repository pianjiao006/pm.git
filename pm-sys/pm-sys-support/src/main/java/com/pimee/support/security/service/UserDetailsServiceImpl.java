package com.pimee.support.security.service;

import com.alibaba.fastjson.JSONObject;
import com.pimee.support.security.filter.NameHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.pimee.common.constants.Constants.UserStatus;
import com.pimee.common.exception.BusinessException;
import com.pimee.common.utils.StringUtils;
import com.pimee.model.vo.SysUserVo;
import com.pimee.service.sys.ISysUserService;
import com.pimee.common.utils.security.LoginUser;

import javax.annotation.Resource;

/**
 * 用户验证处理
 * 
 * @author Bruce Shaw 2020年4月13日 下午5:50:28
 */
@Service
public class UserDetailsServiceImpl implements UserDetailsService {
	private static final Logger log = LoggerFactory.getLogger(UserDetailsServiceImpl.class);

	@Resource
	private ISysUserService sysUserService;

	@Resource
	private SysPermissionService permissionService;

	@Override
	public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
		NameHelper nameHelper= JSONObject.parseObject(userName, NameHelper.class);
		userName = nameHelper.getName();
		// 根据用户类型获取用户信息  然后进行校验
		String userType = nameHelper.getUserType();
		SysUserVo sysUser = sysUserService.selectUserByUserName(userName);
		if (StringUtils.isNull(sysUser)) {
			log.info("登录用户：{} 不存在.", userName);
			throw new UsernameNotFoundException("登录用户：" + userName + " 不存在");
		} else if (UserStatus.DELETED.getCode().equals(sysUser.getDelFlag())) {
			log.info("登录用户：{} 已被删除.", userName);
			throw new BusinessException("对不起，您的账号：" + userName + " 已被删除");
		} else if (UserStatus.DISABLE.getCode().equals(sysUser.getStatus())) {
			log.info("登录用户：{} 已被停用.", userName);
			throw new BusinessException("对不起，您的账号：" + userName + " 已停用");
		}
		return createLoginUser(sysUser);
	}

	public UserDetails createLoginUser(SysUserVo user) {
		return new LoginUser(user, permissionService.getMenuPermission(user));
	}
}
