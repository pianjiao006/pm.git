package com.pimee.support.security.filter;

import java.io.IOException;
import java.util.Objects;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.pimee.common.constants.Constants;
import com.pimee.common.constants.UserConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import com.pimee.common.utils.StringUtils;
import com.pimee.common.utils.security.LoginUser;
import com.pimee.support.security.service.TokenService;
import com.pimee.common.utils.security.SecurityUtils;

/**
 * token过滤器 验证token有效性
 *
 * @author Bruce Shaw 2020年4月14日 上午10:00:59
 */
@Component
public class JwtAuthenticationTokenFilter extends OncePerRequestFilter {
    @Autowired
    private TokenService tokenService;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
            throws ServletException, IOException {
//		LoginUser loginUser = tokenService.getLoginUser(request);
//		if (StringUtils.isNotNull(loginUser) && StringUtils.isNull(SecurityUtils.getAuthentication())) {
//			tokenService.verifyToken(loginUser);
//			UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(loginUser,
//					null, loginUser.getAuthorities());
//			authenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
//			SecurityContextHolder.getContext().setAuthentication(authenticationToken);
//		}
//		chain.doFilter(request, response);
        LoginUser loginUser = tokenService.getLoginUser(request);
        if (!Objects.isNull(loginUser) && Objects.isNull(SecurityUtils.getAuthentication())) {
            /********添加对前端用户类型判断***********/
            String typeString = tokenService.getType(request);
            logger.error(typeString + "：：：：：：：：：：：：类型");
            // 后端用户
            if (typeString.equals(UserConstants.ADMIN)) {
                tokenService.verifyToken(loginUser);
            } else {
                tokenService.verifyToken2(loginUser);
            }
            /********添加部分***********/

            UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(loginUser, null, loginUser.getAuthorities());
            authenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
            SecurityContextHolder.getContext().setAuthentication(authenticationToken);
        }
        chain.doFilter(request, response);
    }
}
