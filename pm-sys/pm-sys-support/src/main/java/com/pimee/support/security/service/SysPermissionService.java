package com.pimee.support.security.service;

import com.pimee.model.vo.SysUserVo;
import com.pimee.service.sys.ISysMenuService;
import com.pimee.service.sys.ISysRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashSet;
import java.util.Set;

/**
 * 用户权限处理
 * 
 * @author Bruce Shaw 2020年4月13日 下午5:17:18
 */
@Service
public class SysPermissionService {
	@Resource
	private ISysRoleService sysRoleService;

	@Resource
	private ISysMenuService sysMenuService;

	/**
	 * 获取角色数据权限
	 * 
	 * @param user
	 *            用户信息
	 * @return 角色权限信息
	 */
	public Set<String> getRolePermission(SysUserVo user) {
		Set<String> roles = new HashSet<String>();
		// 管理员拥有所有权限
		if (user.isAdmin()) {
			roles.add("admin");
		} else {
			roles.addAll(sysRoleService.selectRolePermissionByUserId(user.getUserId()));
		}
		return roles;
	}

	/**
	 * 获取菜单数据权限
	 * 
	 * @param user
	 *            用户信息
	 * @return 菜单权限信息
	 */
	public Set<String> getMenuPermission(SysUserVo user) {
		Set<String> perms = new HashSet<String>();
		// 管理员拥有所有权限
		if (user.isAdmin()) {
			perms.add("*:*:*");
		} else {
			perms.addAll(sysMenuService.selectPermsByUserId(user.getUserId()));
		}
		return perms;
	}

	/**
	 * 获取角色数据权限
	 * 
	 * @param user
	 *            用户信息
	 * @return 角色权限信息
	 */
	public Set<String> getRolePermission1(SysUserVo user) {
		Set<String> roles = new HashSet<String>();
		// 管理员拥有所有权限
		if (user.isAdmin()) {
			roles.add("admin");
		} else {
			roles.addAll(sysRoleService.selectRolePermissionByUserId(user.getUserId()));
		}
		return roles;
	}
}
