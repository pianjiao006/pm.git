package com.pimee.oss;

import com.alibaba.fastjson.JSONObject;
import com.aliyun.oss.ClientException;
import com.aliyun.oss.OSSClient;
import com.aliyun.oss.OSSException;
import com.aliyun.oss.model.OSSObject;
import com.aliyun.oss.model.ObjectMetadata;
import com.aliyun.oss.model.PutObjectRequest;
import com.aliyun.oss.model.PutObjectResult;
import com.pimee.common.core.support.HttpCode;
import com.pimee.common.exception.BusinessException;
import com.pimee.oss.config.OSSClientConfig;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.InputStream;

@Log4j2
@Service
public class AliyunStorageService {

    @Autowired
    private OSSClient ossClient;
    @Resource
    private OSSClientConfig oSSClientConfig;

    /**
     * 上传文件
     * @param fileName
     * @param is
     * @param contentLength
     * @param contentType
     * @return
     */
    public String upload(String fileName, InputStream is, long contentLength, String contentType) {
        ObjectMetadata objectMetadata = new ObjectMetadata();
        objectMetadata.setContentLength(contentLength);
        objectMetadata.setContentType(contentType);
        PutObjectRequest putObjectRequest = new PutObjectRequest(oSSClientConfig.getBucket(), fileName, is, objectMetadata);
        PutObjectResult putObjectResult = ossClient.putObject(putObjectRequest);
        log.info("===> putObjectResult:{}", JSONObject.toJSONString(putObjectResult));
        return oSSClientConfig.getBucketUrl() + fileName;
    }

    /**
     * 校验文件是否存在
     * @param fileName
     * @return
     */
    public boolean checkExist(String fileName){
        boolean exist = ossClient.doesObjectExist(oSSClientConfig.getBucket(), fileName);
        return exist;
    }

    /**
     *
     * @param fileName
     * @return
     */
    public InputStream download(String fileName){
        // ossObject包含文件所在的存储空间名称、文件名称、文件元信息以及一个输入流。
        OSSObject ossObject = null;
        try {
            ossObject = ossClient.getObject(oSSClientConfig.getBucket(), fileName);
            return ossObject.getObjectContent();
        } catch (OSSException e) {
            log.error("===> oss下载文件异常", e);
            throw new BusinessException(HttpCode.INTERNAL_SERVER_ERROR.value(), "oss下载文件异常");
        } catch (ClientException e) {
            log.error("===> oss客户端异常", e);
            throw new BusinessException(HttpCode.INTERNAL_SERVER_ERROR.value(), "oss客户端异常");
        }
    }

    /**
     * 删除文件
     * @param fileName
     */
    public void removeFile(String fileName) {
        try {
            ossClient.deleteObject(oSSClientConfig.getBucket(), fileName);
        } catch (OSSException e) {
            log.error("===> oss删除文件异常", e);
            throw new BusinessException(HttpCode.INTERNAL_SERVER_ERROR.value(), "oss删除文件异常");
        } catch (ClientException e) {
            log.error("===> oss客户端异常", e);
            throw new BusinessException(HttpCode.INTERNAL_SERVER_ERROR.value(), "oss客户端异常");
        }
    }
}
