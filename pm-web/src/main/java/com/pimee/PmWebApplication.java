package com.pimee;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PmWebApplication {
    public static void main(String[] args) {
        SpringApplication.run(PmWebApplication.class, args);
    }
}

