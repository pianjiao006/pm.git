package com.pimee.web.controller.sys;

import com.pimee.common.constants.UserConstants;
import com.pimee.common.core.config.PmConfig;
import com.pimee.common.utils.ServletUtils;
import com.pimee.common.utils.StringUtils;
import com.pimee.common.utils.file.FileUploadUtils;
import com.pimee.common.utils.security.LoginUser;
import com.pimee.common.utils.security.SecurityUtils;
import com.pimee.model.SysUser;
import com.pimee.model.vo.SysUserVo;
import com.pimee.service.sys.ISysUserService;
import com.pimee.support.security.service.TokenService;
import com.pimee.web.controller.model.AjaxResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.IOException;

/**
 * 个人信息 业务处理
 *
 * @author ruoyi
 */
@RestController
@RequestMapping("/system/user/profile")
public class SysProfileController {
    @Resource
    private ISysUserService sysUserService;

    @Resource
    private TokenService tokenService;

    /**
     * 个人信息
     */
    @GetMapping
    public AjaxResult profile() {
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        SysUser user = loginUser.getUser();
        AjaxResult ajax = AjaxResult.success(user);
        ajax.put("roleGroup", sysUserService.selectUserRoleGroup(loginUser.getUsername()));
        ajax.put("postGroup", sysUserService.selectUserPostGroup(loginUser.getUsername()));
        return ajax;
    }

    /**
     * 修改用户
     */
    @PutMapping
    public AjaxResult updateProfile(@RequestBody SysUserVo user) {
        if (StringUtils.isNotEmpty(user.getPhonenumber())
                && UserConstants.NOT_UNIQUE.equals(sysUserService.checkPhoneUnique(user.getPhonenumber()))) {
            return AjaxResult.error("修改用户'" + user.getUserName() + "'失败，手机号码已存在");
        }
        if (StringUtils.isNotEmpty(user.getEmail())
                && UserConstants.NOT_UNIQUE.equals(sysUserService.checkEmailUnique(user.getEmail()))) {
            return AjaxResult.error("修改用户'" + user.getUserName() + "'失败，邮箱账号已存在");
        }
        sysUserService.updateUser(user);

        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        // 更新缓存用户信息
        loginUser.getUser().setNickName(user.getNickName());
        loginUser.getUser().setPhonenumber(user.getPhonenumber());
        loginUser.getUser().setEmail(user.getEmail());
        loginUser.getUser().setSex(user.getSex());
        tokenService.setLoginUser(loginUser);
        return AjaxResult.success();
    }

    /**
     * 重置密码
     */
    @PutMapping("/updatePwd")
    public AjaxResult updatePwd(String oldPassword, String newPassword) {
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        String userName = loginUser.getUsername();
        String password = loginUser.getPassword();
        if (!SecurityUtils.matchesPassword(oldPassword, password)) {
            return AjaxResult.error("修改密码失败，旧密码错误");
        }
        if (SecurityUtils.matchesPassword(newPassword, password)) {
            return AjaxResult.error("新密码不能与旧密码相同");
        }
        String lastPwd = SecurityUtils.encryptPassword(newPassword);
        sysUserService.resetUserPwd(userName, lastPwd);
        // 更新缓存用户密码
        loginUser.getUser().setPassword(lastPwd);
        tokenService.setLoginUser(loginUser);
        return AjaxResult.success();
    }

    /**
     * 头像上传
     */
    @PostMapping("/avatar")
    public AjaxResult avatar(@RequestParam("avatarfile") MultipartFile file) throws IOException {
        if (!file.isEmpty()) {
            LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
            String avatar = FileUploadUtils.upload(PmConfig.getAvatarPath(), file);
            if(sysUserService.updateUserAvatar(loginUser.getUsername(), avatar)) {
                AjaxResult ajax = AjaxResult.success();
                ajax.put("imgUrl", avatar);
                // 更新缓存用户头像
                loginUser.getUser().setAvatar(avatar);
                tokenService.setLoginUser(loginUser);
                return ajax;
            }
        }
        return AjaxResult.error("上传图片异常，请联系管理员");
    }
}
