package com.pimee.web.controller.sys;

import com.github.pagehelper.PageInfo;
import com.pimee.common.utils.ResultUtil;
import com.pimee.common.utils.security.SecurityUtils;
import com.pimee.model.SysDictType;
import com.pimee.service.sys.ISysDictTypeService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * 数据字典信息
 *
 * @author Bruce Shaw
 */
@RestController
@RequestMapping("/system/dict/type")
public class SysDictTypeController {

    @Resource
    private ISysDictTypeService sysDictTypeService;

    /**
     * 获取数据类型分页列表
     */
    @PreAuthorize("@ss.hasPermi('system:dict:list')")
    @GetMapping("/list")
    public Object list(@RequestParam Map<String, Object> params) {
        PageInfo<SysDictType> pageResult = sysDictTypeService.pageList(params);
        return ResultUtil.ok(pageResult);
    }

    /**
     * 根据数据类型编号获取详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:dict:query')")
    @GetMapping(value = "/{dictId}")
    public Object getInfo(@PathVariable Long dictId) {
        return ResultUtil.ok(sysDictTypeService.selectByKey(dictId));
    }

    /**
     * 新增数据类型
     */
    @PreAuthorize("@ss.hasPermi('system:dict:add')")
    @PostMapping
    public Object add(@Validated @RequestBody SysDictType dict) {
        dict.setCreateBy(SecurityUtils.getUsername());
        sysDictTypeService.insertDictType(dict);
        return ResultUtil.ok();
    }

    /**
     * 修改数据类型
     */
    @PreAuthorize("@ss.hasPermi('system:dict:edit')")
    @PutMapping
    public Object edit(@Validated @RequestBody SysDictType dict) {
        dict.setUpdateBy(SecurityUtils.getUsername());
        sysDictTypeService.updateDictType(dict);
        return ResultUtil.ok();
    }

    /**
     * 删除数据类型
     */
    @PreAuthorize("@ss.hasPermi('system:dict:remove')")
    @DeleteMapping("/{dictIds}")
    public Object remove(@PathVariable Long[] dictIds) {
        sysDictTypeService.deleteByIds(dictIds);
        return ResultUtil.ok();
    }

    /**
     * 清空缓存
     */
    @PreAuthorize("@ss.hasPermi('system:dict:remove')")
    @DeleteMapping("/clearCache")
    public Object clearCache() {
        sysDictTypeService.clearCache();
        return ResultUtil.ok();
    }

    /**
     * 获取字典选择框列表
     */
    @GetMapping("/optionselect")
    public Object optionSelect() {
        List<SysDictType> dictTypes = sysDictTypeService.listAll();
        return ResultUtil.ok(dictTypes);
    }

}
