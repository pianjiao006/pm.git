package com.pimee.web.controller.sys;

import com.github.pagehelper.PageInfo;
import com.pimee.common.utils.ResultUtil;
import com.pimee.common.utils.security.SecurityUtils;
import com.pimee.model.SysPost;
import com.pimee.service.sys.ISysPostService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Map;

/**
 * 岗位信息操作处理
 *
 * @author Bruce Shaw
 */
@RestController
@RequestMapping("/system/post")
public class SysPostController {
    @Resource
    private ISysPostService sysPostService;

    /**
     * 获取岗位分页列表
     */
    @PreAuthorize("@ss.hasPermi('system:post:list')")
    @GetMapping("/list")
    public Object list(@RequestParam Map<String, Object> params) {
        PageInfo<SysPost> pagePost = sysPostService.pageList(params);
        return ResultUtil.ok(pagePost);
    }

    /**
     * 根据岗位编号获取详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:post:query')")
    @GetMapping(value = "/{postId}")
    public Object getInfo(@PathVariable Long postId) {
        return ResultUtil.ok(sysPostService.selectByKey(postId));
    }

    /**
     * 新增岗位
     */
    @PreAuthorize("@ss.hasPermi('system:post:add')")
    @PostMapping
    public Object add(@Validated @RequestBody SysPost post) {
        post.setCreateBy(SecurityUtils.getUsername());
        sysPostService.insertPost(post);
        return ResultUtil.ok();
    }

    /**
     * 修改岗位
     */
    @PreAuthorize("@ss.hasPermi('system:post:edit')")
    @PutMapping
    public Object edit(@Validated @RequestBody SysPost post) {
        post.setUpdateBy(SecurityUtils.getUsername());
        sysPostService.updatePost(post);
        return ResultUtil.ok();
    }

    /**
     * 删除岗位
     */
    @PreAuthorize("@ss.hasPermi('system:post:remove')")
    @DeleteMapping("/{postIds}")
    public Object remove(@PathVariable Long[] postIds) {
        sysPostService.deleteByIds(postIds);
        return ResultUtil.ok();
    }


}
