package com.pimee.web.controller.monitor;

import com.github.pagehelper.PageInfo;
import com.pimee.common.aop.annotation.Log;
import com.pimee.common.aop.enums.BusinessType;
import com.pimee.common.utils.ResultUtil;
import com.pimee.service.sys.ISysLoginInForService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Map;

/**
 * 系统访问记录
 *
 * @author Bruce Shaw
 */
@RestController
@RequestMapping("/monitor/logininfor")
public class SysLoginInForController {
    @Resource
    private ISysLoginInForService sysLoginInForService;

    @PreAuthorize("@ss.hasPermi('monitor:logininfor:list')")
    @GetMapping("/list")
    public Object list(@RequestParam Map<String, Object> params) {
        PageInfo<Map<String, Object>> pageList = sysLoginInForService.pageList(params);
        return ResultUtil.ok(pageList);
    }

    @PreAuthorize("@ss.hasPermi('monitor:logininfor:remove')")
    @Log(title = "登录日志", businessType = BusinessType.DELETE)
    @DeleteMapping("/{infoIds}")
    public Object remove(@PathVariable Long[] infoIds) {
        sysLoginInForService.deleteByIds(infoIds);
        return ResultUtil.ok();
    }

    @PreAuthorize("@ss.hasPermi('monitor:logininfor:remove')")
    @Log(title = "登录日志", businessType = BusinessType.CLEAN)
    @DeleteMapping("/clean")
    public Object clean() {
        sysLoginInForService.cleanLoginInFor();
        return ResultUtil.ok();
    }
}
