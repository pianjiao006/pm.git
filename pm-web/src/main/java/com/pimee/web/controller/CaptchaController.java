package com.pimee.web.controller;

import com.google.code.kaptcha.Producer;
import com.google.common.collect.Maps;
import com.pimee.common.constants.Constants;
import com.pimee.common.utils.IdUtils;
import com.pimee.common.utils.RedisUtil;
import com.pimee.common.utils.sign.Base64;
import com.pimee.web.controller.model.AjaxResult;
import org.apache.http.HttpStatus;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.FastByteArrayOutputStream;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Map;

/**
 * 验证码操作处理
 * 
 * @author Bruce Shaw 2020年4月14日 上午10:52:32
 */
@RestController
public class CaptchaController {
	@Resource
	private RedisUtil<String> redisUtil;
	// 验证码类型
    @Value("${pm.captchaType}")
    private String captchaType;
    @Resource(name = "captchaProducer")
    private Producer captchaProducer;
    @Resource(name = "captchaProducerMath")
    private Producer captchaProducerMath;

	/**
	 * 生成验证码
	 */
	@GetMapping("/captchaImage")
	public Object getCode(HttpServletResponse response) throws IOException {
		// 生成随机字串
//		String verifyCode = VerifyCodeUtils.generateVerifyCode(4);
		// 唯一标识
		String uuid = IdUtils.simpleUUID();
		String verifyKey = Constants.CAPTCHA_CODE_KEY + uuid;
		String capStr = null, code = null;
		BufferedImage image = null;
		// 生成验证码
        if ("math".equals(captchaType))
        {
            String capText = captchaProducerMath.createText();
            capStr = capText.substring(0, capText.lastIndexOf("@"));
            code = capText.substring(capText.lastIndexOf("@") + 1);
            image = captchaProducerMath.createImage(capStr);
        }
        else if ("char".equals(captchaType))
        {
            capStr = code = captchaProducer.createText();
            image = captchaProducer.createImage(capStr);
        }
		redisUtil.string_setExpireInMin(verifyKey, code, Constants.CAPTCHA_EXPIRATION);
		 // 转换流信息写出
        FastByteArrayOutputStream os = new FastByteArrayOutputStream();
        try
        {
            ImageIO.write(image, "jpg", os);
        }
        catch (IOException e)
        {
            return AjaxResult.error(e.getMessage());
        }
		Map<String, Object> result = Maps.newHashMap();
		try {
			result.put("code", HttpStatus.SC_OK);
			result.put("msg", "操作成功");
			result.put("uuid", uuid);
			result.put("img", Base64.encode(os.toByteArray()));
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			result.put("code", HttpStatus.SC_INTERNAL_SERVER_ERROR);
			result.put("msg", e.getMessage());
			return result;
		} finally {
			os.close();
		}
	}
}
