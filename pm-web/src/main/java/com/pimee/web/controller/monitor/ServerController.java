package com.pimee.web.controller.monitor;

import com.pimee.common.model.server.Server;
import com.pimee.common.utils.ResultUtil;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 服务器监控
 *
 * @author Bruce Shaw
 */
@RestController
@RequestMapping("/monitor/server")
public class ServerController {
    @PreAuthorize("@ss.hasPermi('monitor:server:list')")
    @GetMapping()
    public Object getInfo() throws Exception {
        Server server = new Server();
        server.copyTo();
        return ResultUtil.ok(server);
    }
}
