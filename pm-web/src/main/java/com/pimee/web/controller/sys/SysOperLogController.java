package com.pimee.web.controller.sys;

import com.github.pagehelper.PageInfo;
import com.pimee.common.utils.ResultUtil;
import com.pimee.service.sys.ISysOperLogService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Map;

/**
 * 操作日志记录
 *
 * @author Bruce Shaw
 */
@RestController
@RequestMapping("/monitor/operlog")
public class SysOperLogController {
    @Resource
    private ISysOperLogService sysOperLogService;

    /**
     * 获取操作日志分页列表
     */
    @PreAuthorize("@ss.hasPermi('system:operlog:list')")
    @GetMapping("/list")
    public Object list(@RequestParam Map<String, Object> params) {
        PageInfo<Map<String, Object>> pageList = sysOperLogService.pageList(params);
        return ResultUtil.ok(pageList);
    }

    /**
     * 根据操作日志编号获取详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:operlog:query')")
    @GetMapping(value = "/{operId}")
    public Object getInfo(@PathVariable Long operId) {
        return ResultUtil.ok(sysOperLogService.selectByKey(operId));
    }

    /**
     * 删除操作日志
     */
    @PreAuthorize("@ss.hasPermi('system:operlog:remove')")
    @DeleteMapping("/{operIds}")
    public Object remove(@PathVariable Long[] operIds) {
        sysOperLogService.deleteByIds(operIds);
        return ResultUtil.ok();
    }

    @PreAuthorize("@ss.hasPermi('monitor:operlog:remove')")
    @DeleteMapping("/clean")
    public Object clean() {
        sysOperLogService.cleanOperLog();
        return ResultUtil.ok();
    }
}
