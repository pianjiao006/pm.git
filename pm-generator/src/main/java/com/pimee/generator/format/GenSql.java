package com.pimee.generator.format;

import java.sql.*;

/**
 * 把数据库原生带下划线的字段转换成驼峰式，可直接使用于mybatis的xmlMapper文件
 * 例如字段 dept_id 那么最后经过转换成为dept_id deptId
 *
 */
public class GenSql {

    // 连接数据库的 url：jdbc协议:数据库子协议://主机:端口/数据库
    private static String url = "jdbc:mysql://localhost:3306/cxx-supply-od?generateSimpleParameterMetadata=true&useUnicode=true&characterEncoding=UTF8";
    private static String userName = "root";   // 用户名
    private static String password = "123456";   // 密码

    public static void main(String[] args) throws SQLException {
        String formatTableName = "bs_order_complain";// 设置表名
        String aliasTableName = "a"; // 设置表的别名
        genTransSql(url, userName, password, formatTableName, aliasTableName);
    }

    /**
     * 产生转换sql
     * @param formatTableName
     * @param aliasTableName
     * @return
     * @throws SQLException
     */
    public static String genTransSql(String url, String userName, String password, String formatTableName, String aliasTableName) throws SQLException {
        // 获取数据库连接对象
        Connection conn = JdbcUtils.getConnection(url, userName, password);
        // 获取数据库元数据
        DatabaseMetaData metaData = conn.getMetaData();
        String[] types = {"TABLE"};
        ResultSet rs = metaData.getTables(null, null, "%", types);
        boolean aliasIsSet = aliasTableName.length() > 0;
        StringBuilder stringBuilder = new StringBuilder("SELECT ");
        while (rs.next()) {
            //3对应的位置就是表名
            String tableName = rs.getString(3);
            if (formatTableName.equals(tableName)) {
                //根据表名用sql查询，但无需获得数据，只需要元数据即可
                String sql = "SELECT * FROM " + tableName + " WHERE 1 = 0";
                Statement stmt = conn.createStatement();
                ResultSet results = stmt.executeQuery(sql);
                //获得sql查询的元数据并打印
                ResultSetMetaData resultSetMetaData = results.getMetaData();
                for (int i = 1; i <= resultSetMetaData.getColumnCount(); i++) {
                    String columnName = resultSetMetaData.getColumnName(i);
                    String formatColumnName = formatColumn(columnName);
                    if(aliasIsSet){
                        stringBuilder.append(aliasTableName).append(".").append(formatColumnName).append(",");
                    }else{
                        stringBuilder.append(formatColumnName).append(",");
                    }
                }
            }
        }
        String baseSql = stringBuilder.toString();
        String lastTransSql = baseSql.substring(0, baseSql.length() - 1) +
                " FROM " + formatTableName + (aliasIsSet ? (" " + aliasTableName) : "");
        System.out.println("===> 拷贝下面语句用于xmlMapper: ");
        System.out.println(lastTransSql);
        return lastTransSql;
    }

    /**
     * 转换字段带下划线“_”为驼峰式，然后拼接返回
     * @param formatReplaceStr
     * @return
     */
    public static String formatColumn(String formatReplaceStr) {
        if(formatReplaceStr.indexOf("_") < 0){
            return formatReplaceStr;
        }
        String resultStr = genFirstLowCamelPatternWord(formatReplaceStr);
        String resultColumn = formatReplaceStr + " " + resultStr;
        return resultColumn;
    }

    /**
     * 第一个字母小写的驼峰式
     * @param formatReplaceStr
     * @return
     */
    public static String genFirstLowCamelPatternWord(String formatReplaceStr){
        String resultStr = genCamelPatternWholeWord(formatReplaceStr);
        resultStr = resultStr.substring(0,1).toLowerCase()+resultStr.substring(1);
        return resultStr;
    }

    /**
     * 全部是驼峰式
     * @param formatReplaceStr
     * @return
     */
    public static String genCamelPatternWholeWord(String formatReplaceStr){
        String[] formatReplaceStrArr = formatReplaceStr.split("_");
        // 如果没有包含下划线 那么直接返回整个字段
        if(formatReplaceStrArr.length == 1){
            return formatReplaceStr;
        }
        int i = 0;
        String[] replaceArr = new String[formatReplaceStrArr.length];
        for (String replaceStr : formatReplaceStrArr) {
            i++;
            String tempStr = replaceStr.substring(0, 1).toUpperCase() + replaceStr.substring(1, replaceStr.length());
            replaceArr[i - 1] = tempStr;
        }

        String resultStr = "";
        for (String tempReplaceStr : replaceArr) {
            resultStr += tempReplaceStr;
        }
        return resultStr;
    }
}
