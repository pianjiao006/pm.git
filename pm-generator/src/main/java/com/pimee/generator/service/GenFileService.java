package com.pimee.generator.service;

import com.pimee.generator.format.GenSql;
import com.pimee.generator.format.JdbcUtils;
import com.pimee.generator.model.FieldInfo;
import com.pimee.generator.model.TemplateConfig;
import com.pimee.generator.util.BeanUtil;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import java.io.File;
import java.io.PrintWriter;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Log4j2
@Service
public class GenFileService {
    public String backBasePath;
    public String frontBasePath;
    @Value("${jdbc_url}")
    private String url;
    @Value("${jdbc_user}")
    private String userName;
    @Value("${jdbc_password}")
    private String password;

    @Autowired
    private TemplateEngine templateEngine;
    private TemplateConfig templateConfig;

    public void initData(String tableName) {
        templateConfig = new TemplateConfig();
        try {
            // 获取数据库连接对象
            Connection conn = JdbcUtils.getConnection(url, userName, password);
            // 获取数据库元数据
            DatabaseMetaData metaData = conn.getMetaData();
            String[] types = {"TABLE"};
            ResultSet rs = metaData.getTables(null, null, tableName, types);
            while (rs.next()) {
                String tableRemark = rs.getString("REMARKS");// 表备注
                templateConfig.setTableRemark(tableRemark);
            }
            Statement stmt = conn.createStatement();
            ResultSet keySet = metaData.getPrimaryKeys(null, "%", tableName);
            String keyProperty = "";// 主键的字段名称
            while (keySet.next()) {
                keyProperty = keySet.getString("COLUMN_NAME");
            }

            List<FieldInfo> fieldList = new ArrayList<>();
            Set<String> importPackageSet = new HashSet<>();
            // 获取表的相对应的列的名字
            ResultSet tableInfo = metaData.getColumns(null, "%", tableName, "%");
            while (tableInfo.next()) {
                FieldInfo fieldInfo = new FieldInfo();
                String columnName = tableInfo.getString("COLUMN_NAME"); // 列的名称
                if(keyProperty.equals(columnName)){
                    fieldInfo.setKeyFlag(true);
                }
                String typeName = tableInfo.getString("TYPE_NAME"); // 字段的类型
                String remark = tableInfo.getString("REMARKS"); // 备注
                String propertyName = GenSql.genFirstLowCamelPatternWord(columnName);
                String propertyNameCamelWhole = GenSql.genCamelPatternWholeWord(columnName);
                fieldInfo.setJdbcType(typeName);
                fieldInfo.setColumnName(columnName);
                // 如果第一个单词小写 那么就转换为大写
                fieldInfo.setPropertyNameCamelWhole(propertyNameCamelWhole.substring(0,1).toUpperCase()+propertyNameCamelWhole.substring(1));
                fieldInfo.setPropertyName(propertyName);
                fieldInfo.setRemark(remark);
                fieldList.add(fieldInfo);
            }

            //根据表名用sql查询，但无需获得数据，只需要元数据即可
            String sql = "SELECT * FROM " + tableName;
            ResultSet results = stmt.executeQuery(sql);
            //获得sql查询的元数据并打印
            ResultSetMetaData resultSetMetaData = results.getMetaData();
            for (FieldInfo fieldInfo : fieldList){
                for (int i = 1; i <= resultSetMetaData.getColumnCount(); i++) {
                    String columnName = resultSetMetaData.getColumnName(i);
                    if(fieldInfo.getColumnName().equals(columnName)){
                        String columnClassName = resultSetMetaData.getColumnClassName(i);
                        String firstTypeName = columnClassName.substring(0, columnClassName.lastIndexOf("."));
                        String lastTypeName = columnClassName.substring(columnClassName.lastIndexOf(".")+1);
                        if("Timestamp".equals(lastTypeName)){
                            lastTypeName = "Date";
                            importPackageSet.add("java.util.Date");
                            fieldInfo.setPropertyType(lastTypeName);
                            fieldInfo.setJdbcType(lastTypeName.toUpperCase());
                            continue;
                        }else if("Integer".equals(lastTypeName)){
                            fieldInfo.setJdbcType(lastTypeName.toUpperCase());
                        }
                        fieldInfo.setPropertyType(lastTypeName);
                        // 如果是java.lang主包 无需在类中import
                        if("java.lang".equals(firstTypeName)){
                            continue;
                        }
                        importPackageSet.add(columnClassName); // 把引入包的类型添加到集合
                    }
                }
            }

            String selectSql = GenSql.genTransSql(url, userName, password, tableName, "a");
            templateConfig.setMapperExtSelectSql(selectSql);
            templateConfig.setTableName(tableName);
            templateConfig.setImportPackageSet(importPackageSet);

            String entityName = GenSql.genCamelPatternWholeWord(tableName);
            String basePackage = "com.pimee.bs.";
            int prefixLength = tableName.indexOf("_");//
            System.out.println("prefixLength:" + prefixLength);
            String tablePrefix = tableName.substring(0,prefixLength);
            templateConfig.setTablePrefix(tablePrefix);
            // entity
            templateConfig.setEntityPackage(basePackage + "model");
            templateConfig.setEntityTemplateName("model.java.html");
            templateConfig.setEntityName(entityName);
            templateConfig.setEntityNameNoPrefix(entityName.substring(prefixLength));
            templateConfig.setEntityFileName(entityName + ".java");
            templateConfig.setFirstLowEntityNameNoPrefix(entityName.substring(prefixLength,3).toLowerCase()+entityName.substring(3));
            templateConfig.setEntityFileOutputPath(this.transJavaFileToOutputPath(templateConfig.getEntityPackage()));
            // mapper
            templateConfig.setMapperXmlTemplateName("mapper.xml.html");
            templateConfig.setMapperJavaTemplateName("mapper.java.html");
            templateConfig.setMapperPackage(basePackage + "mapper");
            templateConfig.setMapperName(entityName+"Mapper");
            templateConfig.setMapperXmlFileName(entityName + "Mapper.xml");
            templateConfig.setMapperJavaFileName(entityName + "Mapper.java");
            templateConfig.setMapperJavaFileOutputPath(this.transJavaFileToOutputPath(templateConfig.getMapperPackage()));
            templateConfig.setMapperXmlFileOutputPath(this.transMapperXmlFileToOutputPath());
            // mapperExt
            templateConfig.setMapperExtPackage(basePackage + "mapper.ext");
            templateConfig.setMapperExtName("Ext"+ entityName + "Mapper");
            templateConfig.setMapperExtXmlTemplateName("mapperExt.xml.html");
            templateConfig.setMapperExtXmlFileName("Ext"+ entityName + "Mapper.xml");
            templateConfig.setMapperExtJavaTemplateName("mapperExt.java.html");
            templateConfig.setMapperExtJavaFileName("Ext" + entityName + "Mapper.java");
            templateConfig.setMapperExtJavaFileOutputPath(this.transJavaFileToOutputPath(templateConfig.getMapperExtPackage()));
            templateConfig.setMapperExtXmlFileOutputPath(this.transMapperExtXmlFileToOutputPath());
            // serviceInterface
            templateConfig.setServiceInterfacePackage(basePackage + "service");
            templateConfig.setServiceInterfaceTemplateName("serviceInterface.java.html");
            templateConfig.setServiceInterfaceName("I" + entityName + "Service");
            templateConfig.setServiceInterfaceFileName("I" + entityName + "Service.java");
            templateConfig.setServiceInterfaceFileOutputPath(this.transJavaFileToOutputPath(templateConfig.getServiceInterfacePackage()));
            // service
            templateConfig.setServicePackage(basePackage + "service.impl");
            templateConfig.setServiceTemplateName("service.java.html");
            templateConfig.setServiceName(entityName + "Service");
            templateConfig.setServiceFileName(entityName + "Service.java");
            templateConfig.setFirstLowServiceName(entityName.substring(0,1).toLowerCase()+entityName.substring(1)+"Service");
            templateConfig.setServiceFileOutputPath(this.transJavaFileToOutputPath(templateConfig.getServicePackage()));
            // controller
            templateConfig.setControllerPackage(basePackage + "web");
            templateConfig.setControllerTemplateName("controller.java.html");
            templateConfig.setControllerName(entityName + "Controller");
            templateConfig.setControllerFileName(entityName + "Controller.java");
            templateConfig.setControllerFileOutputPath(this.transJavaFileToOutputPath(templateConfig.getControllerPackage()));
            // vue
            templateConfig.setVueTemplateName("front/index.vue.html");
            templateConfig.setVueFileName(templateConfig.getFirstLowEntityNameNoPrefix()+".vue");
            templateConfig.setVueFileOutputPath(this.transVueFileToOutputPath());
            // js
            templateConfig.setJsTemplateName("front/index.js.html");
            templateConfig.setJsFileName(templateConfig.getFirstLowEntityNameNoPrefix()+".js");
            templateConfig.setJsFileOutputPath(this.transJsFileToOutputPath());

            templateConfig.setFieldList(fieldList);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * 生成文件
     *
     * @throws Exception
     */
    public void createFile(String templateName, String outFileName, String outputPath) {
        // 上下文
        Context context = new Context();
        context.setVariables(BeanUtil.getBeanPropertyMap(this.templateConfig));
        File destPath = new File(outputPath);
        if(!destPath.exists()){
            destPath.mkdirs();
        }
        // 输出流
        File dest = new File(outputPath, outFileName);
        if (dest.exists()) {
            dest.delete();
        }
        try (PrintWriter writer = new PrintWriter(dest, "UTF-8")) {
            // 生成html
            templateEngine.process(templateName, context, writer);
        } catch (Exception e) {
            log.error("[静态页服务]：生成静态页异常", e);
        }
    }

    /**
     * 生成java文件的输出路径
     * @param packagePath
     * @return
     */
    private String transJavaFileToOutputPath(String packagePath){
        packagePath = packagePath.replaceAll("\\.", "/")+"/";
        return backBasePath + "/src/main/java/" + packagePath;
    }

    /**
     * 生成Mapper文件的输出路径
     * @return
     */
    private String transMapperXmlFileToOutputPath(){
        return backBasePath + "/src/main/resources/mapper";
    }

    /**
     * 生成MapperExt文件的输出路径
     * @return
     */
    private String transMapperExtXmlFileToOutputPath(){
        return backBasePath + "/src/main/resources/mapper/ext";
    }

    /**
     * vue
     * @return
     */
    private String transVueFileToOutputPath(){
        return frontBasePath + "/template/bs";
    }

    /**
     * js
     * @return
     */
    private String transJsFileToOutputPath(){
        return frontBasePath + "/template/bs";
    }
    /**
     * 生成模版
     * @param tableName
     */
    public void genFileFromTemplate(String tableName, boolean overrideMapperExtXml){
        this.initData(tableName);
        this.createFile(templateConfig.getEntityTemplateName(), templateConfig.getEntityFileName(), templateConfig.getEntityFileOutputPath());
        this.createFile(templateConfig.getMapperJavaTemplateName(), templateConfig.getMapperJavaFileName(), templateConfig.getMapperJavaFileOutputPath());
        this.createFile(templateConfig.getMapperXmlTemplateName(), templateConfig.getMapperXmlFileName(), templateConfig.getMapperXmlFileOutputPath());
        if(overrideMapperExtXml){ // 是否覆盖ext的xml文件
            this.createFile(templateConfig.getMapperExtXmlTemplateName(), templateConfig.getMapperExtXmlFileName(), templateConfig.getMapperExtXmlFileOutputPath());
        }
        this.createFile(templateConfig.getMapperExtJavaTemplateName(), templateConfig.getMapperExtJavaFileName(), templateConfig.getMapperExtJavaFileOutputPath());
        this.createFile(templateConfig.getServiceTemplateName(), templateConfig.getServiceFileName(), templateConfig.getServiceFileOutputPath());
        this.createFile(templateConfig.getServiceInterfaceTemplateName(), templateConfig.getServiceInterfaceFileName(), templateConfig.getServiceInterfaceFileOutputPath());
        this.createFile(templateConfig.getControllerTemplateName(), templateConfig.getControllerFileName(), templateConfig.getControllerFileOutputPath());

        // static file
        this.createFile(templateConfig.getVueTemplateName(), templateConfig.getVueFileName(), templateConfig.getVueFileOutputPath());
        this.createFile(templateConfig.getJsTemplateName(), templateConfig.getJsFileName(), templateConfig.getJsFileOutputPath());
    }

    /**
     * 生成后端文件
     * @param tableName
     * @param backBasePath
     * @param overrideMapperExtXml 是否覆盖ext的xml文件 一般不建议覆盖 因为有可能ext文件写了很多关联多表的操作
     */
    public void genFiles(String tableName, String backBasePath, String frontBasePath, boolean overrideMapperExtXml){
        this.backBasePath = backBasePath;
        this.frontBasePath = frontBasePath;
        String[] tableNames = tableName.split(",");
        for (String table : tableNames) {
            this.genFileFromTemplate(table, overrideMapperExtXml);
        }
    }
}
