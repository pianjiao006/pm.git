package com.pimee.generator.model;

import lombok.Data;

import java.util.List;
import java.util.Set;

@Data
public class TemplateConfig {
    private String tableName;// 表名
    private String tablePrefix;// 前缀
    private String tableRemark;// 表备注

    private String mapperXmlTemplateName;// mapperXml模板名称
    private String mapperJavaTemplateName;// mapperJava模板名称
    private String mapperPackage;// mapper包名
    private String mapperName; // mapper名称
    private String mapperXmlFileName; // mapperXml文件名称
    private String mapperJavaFileName; // mapperJava文件名称
    private String mapperXmlFileOutputPath; // mapperXml文件输出路径
    private String mapperJavaFileOutputPath; // mapperJava文件输出路径

    private String mapperExtXmlTemplateName;// mapperExtXml模板名称
    private String mapperExtJavaTemplateName;// mapperExtJav
    private String mapperExtName; // mapperExt名称模板名称
    private String mapperExtPackage;// mapperExt包名
    private String mapperExtXmlFileName; // mapperExtXml文件名称
    private String mapperExtJavaFileName; // mapperExtJava文件名称
    private String mapperExtSelectSql; // 扩展查询语句
    private String mapperExtXmlFileOutputPath; // mapperExtXml文件输出路径
    private String mapperExtJavaFileOutputPath; // mapperExtJava文件输出路径

    private String entityTemplateName;// entity模板名称
    private String entityName; // entity名称
    private String entityNameNoPrefix; // entity名称不带前缀
    private String firstLowEntityNameNoPrefix; // entity名称:
    private String entityPackage;// entity包名
    private String entityFileName; // entity文件名称
    private String entityFileOutputPath; // entity输出路径

    private String serviceTemplateName;// service模板名称
    private String serviceName; // service名称
    private String firstLowServiceName; // service文件名称
    private String servicePackage;// service包名
    private String serviceFileName; // service文件名称
    private String serviceFileOutputPath; // service输出路径

    private String serviceInterfaceTemplateName;// serviceInterface模板名称
    private String serviceInterfaceName; // serviceInterface名称
    private String serviceInterfacePackage;// serviceInterface包名
    private String serviceInterfaceFileName; // serviceInterface文件名称
    private String serviceInterfaceFileOutputPath; // serviceInterface输出路径

    private String controllerTemplateName;// service模板名称
    private String controllerName; // service名称
    private String controllerPackage;// service包名
    private String controllerFileName; // service文件名称
    private String controllerFileOutputPath; // controller输出路径

    private String vueTemplateName;// vue模板名称
    private String vueFileName; // vue文件名称
    private String vueFileOutputPath; // vue输出路径

    private String jsTemplateName;// js模板名称
    private String jsFileName; // js文件名称
    private String jsFileOutputPath; // js输出路径

    private List<FieldInfo> fieldList; // 数据库表数据
    private Set<String> importPackageSet; // 引入包集合

}