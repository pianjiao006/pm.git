package com.pimee.job.web;

import com.github.pagehelper.PageInfo;
import com.pimee.common.aop.annotation.Log;
import com.pimee.common.aop.enums.BusinessType;
import com.pimee.common.core.support.HttpCode;
import com.pimee.common.exception.BusinessException;
import com.pimee.common.utils.ResultUtil;
import com.pimee.common.utils.security.SecurityUtils;
import com.pimee.job.model.SysJob;
import com.pimee.job.service.ISysJobService;
import com.pimee.job.util.CronUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Map;

/**
 * 调度任务信息操作处理
 *
 * @author ruoyi
 */
@RestController
@RequestMapping("/monitor/job")
public class SysJobController {
    @Resource
    private ISysJobService sysJobService;

    /**
     * 查询定时任务列表
     */
    @PreAuthorize("@ss.hasPermi('monitor:job:list')")
    @GetMapping("/list")
    public Object list(@RequestParam Map<String, Object> param) {
        PageInfo<SysJob> pageList = sysJobService.pageList(param);
        return ResultUtil.ok(pageList);
    }

    /**
     * 获取定时任务详细信息
     */
    @PreAuthorize("@ss.hasPermi('monitor:job:query')")
    @GetMapping(value = "/{jobId}")
    public Object getInfo(@PathVariable("jobId") Long jobId) {
        return ResultUtil.ok(sysJobService.selectByKey(jobId));
    }

    /**
     * 新增定时任务
     */
    @PreAuthorize("@ss.hasPermi('monitor:job:add')")
    @PostMapping
    public Object add(@RequestBody SysJob sysJob) {
        if (!CronUtils.isValid(sysJob.getCronExpression())) {
            throw new BusinessException(HttpCode.INTERNAL_SERVER_ERROR.value(), "cron表达式不正确");
        }
        sysJob.setCreateBy(SecurityUtils.getUsername());
        sysJobService.insertJob(sysJob);
        return ResultUtil.ok();
    }

    /**
     * 修改定时任务
     */
    @PreAuthorize("@ss.hasPermi('monitor:job:edit')")
    @Log(title = "定时任务", businessType = BusinessType.UPDATE)
    @PutMapping
    public Object edit(@RequestBody SysJob sysJob) {
        if (!CronUtils.isValid(sysJob.getCronExpression())) {
            throw new BusinessException(HttpCode.INTERNAL_SERVER_ERROR.value(), "cron表达式不正确");
        }
        sysJob.setUpdateBy(SecurityUtils.getUsername());
        sysJobService.updateJob(sysJob);
        return ResultUtil.ok();
    }

    /**
     * 定时任务状态修改
     */
    @PreAuthorize("@ss.hasPermi('monitor:job:changeStatus')")
    @Log(title = "定时任务", businessType = BusinessType.UPDATE)
    @PutMapping("/changeStatus")
    public Object changeStatus(@RequestBody SysJob job) {
        SysJob newJob = sysJobService.selectByKey(job.getJobId());
        newJob.setStatus(job.getStatus());
        sysJobService.changeStatus(newJob);
        return ResultUtil.ok();
    }

    /**
     * 定时任务立即执行一次
     */
    @PreAuthorize("@ss.hasPermi('monitor:job:changeStatus')")
    @Log(title = "定时任务", businessType = BusinessType.UPDATE)
    @PutMapping("/run")
    public Object run(@RequestBody SysJob job) {
        sysJobService.run(job);
        return ResultUtil.ok();
    }

    /**
     * 删除定时任务
     */
    @PreAuthorize("@ss.hasPermi('monitor:job:remove')")
    @Log(title = "定时任务", businessType = BusinessType.DELETE)
    @DeleteMapping("/{jobIds}")
    public Object remove(@PathVariable Long[] jobIds){
        sysJobService.deleteByIds(jobIds);
        return ResultUtil.ok();
    }
}
