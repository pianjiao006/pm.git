package com.pimee.job.service.impl;

import com.github.pagehelper.PageInfo;
import com.pimee.common.constants.Constants.Schedule;
import com.pimee.common.constants.Constants.ScheduleStatus;
import com.pimee.common.core.support.HttpCode;
import com.pimee.common.exception.BusinessException;
import com.pimee.common.service.impl.BaseService;
import com.pimee.common.utils.DateUtil;
import com.pimee.job.mapper.ext.ExtSysJobMapper;
import com.pimee.job.model.SysJob;
import com.pimee.job.service.ISysJobService;
import com.pimee.job.util.CronUtils;
import com.pimee.job.util.ScheduleUtils;
import lombok.extern.slf4j.Slf4j;
import org.quartz.JobDataMap;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
public class SysJobService extends BaseService<SysJob> implements ISysJobService {

	@Resource
	private ExtSysJobMapper extSysJobMapper;
	@Resource
	private Scheduler scheduler;

	@Override
	public PageInfo<SysJob> pageList(Map<String, Object> params) {
		log.info("===> 任务列表分页查询...");
		startPage(params);// 设置分页
		List<SysJob> list = extSysJobMapper.pageList(params);
		return new PageInfo<>(list);
	}

	/**
	 * 暂停任务
	 * 
	 * @param job
	 */
	@Override
	@Transactional
	public void pauseJob(SysJob job) {
		try {
			Long jobId = job.getJobId();
			String jobGroup = job.getJobGroup();
			job.setStatus(ScheduleStatus.PAUSE.getCode());
			int result = this.updateNotNull(job);
			if (result > 0) {
				scheduler.pauseJob(ScheduleUtils.getJobKey(jobId, jobGroup));
			}
		} catch (SchedulerException e) {
			throw new BusinessException("定时任务停止出现异常");
		}
	}

	/**
	 * 恢复任务
	 * 
	 * @param job
	 *            调度信息
	 */
	@Override
	@Transactional
	public void resumeJob(SysJob job) {
		try {
			Long jobId = job.getJobId();
			String jobGroup = job.getJobGroup();
			job.setStatus(ScheduleStatus.NORMAL.getCode());
			int rows = this.updateNotNull(job);
			if (rows > 0) {
				scheduler.resumeJob(ScheduleUtils.getJobKey(jobId, jobGroup));
			}
		} catch (SchedulerException e) {
			throw new BusinessException("定时任务恢复出现异常");
		}
	}

	/**
	 * 删除任务后，所对应的trigger也将被删除
	 * 
	 * @param job
	 *            调度信息
	 */
	@Override
	@Transactional
	public int deleteJob(SysJob job) {
		Long jobId = job.getJobId();
		String jobGroup = job.getJobGroup();
		int rows = this.deleteByKey(jobId);
		if (rows > 0) {
			try {
				scheduler.deleteJob(ScheduleUtils.getJobKey(jobId, jobGroup));
			} catch (SchedulerException e) {
				log.info(e.getMessage(), e);
				throw new BusinessException("删除调度任务异常");
			}
		}
		return rows;
	}

	/**
	 * 批量删除调度信息
	 * 
	 * @param jobIds
	 *            需要删除的数据ID
	 * @return 结果
	 */
	@Override
	@Transactional
	public void deleteByIds(Long[] jobIds) {
		if (jobIds == null || jobIds.length == 0) {
			throw new BusinessException(HttpCode.INTERNAL_SERVER_ERROR.value(), "没有传入任何要删除的标识");
		}
		Example example = new Example(SysJob.class);
		List<Long> idList = Arrays.asList(jobIds);
		example.createCriteria().andIn("jobId", idList);
		this.deleteBy(example);
	}

	@Override
	@Transactional
	public void deleteJobById(Long jobId) {
		SysJob job = this.selectByKey(jobId);
		deleteJob(job);
	}

	/**
	 * 任务调度状态修改
	 * 
	 * @param job
	 *            调度信息
	 */
	@Override
	@Transactional
	public void changeStatus(SysJob job) {
		String status = job.getStatus();
		if (ScheduleStatus.NORMAL.getCode().equals(status)) {
			resumeJob(job);
		} else if (ScheduleStatus.PAUSE.getCode().equals(status)) {
			pauseJob(job);
		}
	}

	/**
	 * 立即运行任务
	 * 
	 * @param job
	 *            调度信息
	 */
	@Override
	@Transactional
	public void run(SysJob job) {
		Long jobId = job.getJobId();
		String jobGroup = job.getJobGroup();
		SysJob properties = this.selectByKey(job.getJobId());
		// 参数
		JobDataMap dataMap = new JobDataMap();
		dataMap.put(Schedule.TASK_PROPERTIES, properties);
		try {
			scheduler.triggerJob(ScheduleUtils.getJobKey(jobId, jobGroup), dataMap);
		} catch (SchedulerException e) {
			log.info(e.getMessage(), e);
			throw new BusinessException("立即执行任务异常");
		}
	}

	/**
	 * 新增任务
	 * 
	 * @param job
	 *            调度信息 调度信息
	 */
	@Override
	@Transactional
	public int insertJob(SysJob job) {
		job.setStatus(ScheduleStatus.PAUSE.getCode());
		int rows = this.saveNotNull(job);
		if (rows > 0) {
			try {
				ScheduleUtils.createScheduleJob(scheduler, job);
			} catch (SchedulerException e) {
				log.info(e.getMessage(), e);
				throw new BusinessException("新增任务异常");
			}
		}
		return rows;
	}

	/**
	 * 更新任务的时间表达式
	 * 
	 * @param job
	 *            调度信息
	 */
	@Override
	@Transactional
	public int updateJob(SysJob job) {
		SysJob properties = this.selectByKey(job.getJobId());
		job.setUpdateTime(DateUtil.now());
		int rows = this.updateNotNull(job);
		if (rows > 0) {
			updateSchedulerJob(job, properties.getJobGroup());
		}
		return rows;
	}

	/**
	 * 更新任务
	 * 
	 * @param job
	 *            调度信息
	 * @param jobGroup
	 *            任务组名
	 */
	public void updateSchedulerJob(SysJob job, String jobGroup) {
		Long jobId = job.getJobId();
		try {
			// 判断是否存在
			JobKey jobKey = ScheduleUtils.getJobKey(jobId, jobGroup);
			if (scheduler.checkExists(jobKey)) {
				// 防止创建时存在数据问题 先移除，然后在执行创建操作
				scheduler.deleteJob(jobKey);
			}
			ScheduleUtils.createScheduleJob(scheduler, job);
		} catch (SchedulerException e) {
			log.info(e.getMessage(), e);
			throw new BusinessException("更新调度信息异常");
		}
	}

	/**
	 * 校验cron表达式是否有效
	 * 
	 * @param cronExpression
	 *            表达式
	 * @return 结果
	 */
	@Override
	public boolean checkCronExpressionIsValid(String cronExpression) {
		return CronUtils.isValid(cronExpression);
	}

}
