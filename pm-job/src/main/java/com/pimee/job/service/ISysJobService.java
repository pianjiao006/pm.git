package com.pimee.job.service;

import com.github.pagehelper.PageInfo;
import com.pimee.common.service.IBaseService;
import com.pimee.job.model.SysJob;

import java.util.Map;

public interface ISysJobService extends IBaseService<SysJob> {
	/**
	 * 分页列表
	 * 
	 * @param params
	 * @return
	 */
	PageInfo<SysJob> pageList(Map<String, Object> params);

	/**
	 * 执行任务
	 * 
	 * @param job
	 */
	void run(SysJob job);

	/**
	 * 校验cron表达式是否有效
	 * 
	 * @param cronExpression
	 * @return
	 */
	boolean checkCronExpressionIsValid(String cronExpression);

	/**
	 * 更新调度任务信息
	 * 
	 * @param job
	 * @return
	 */
	int updateJob(SysJob job);

	/**
	 * 新增调度任务
	 * 
	 * @param job
	 * @return
	 */
	int insertJob(SysJob job);

	/**
	 * 修改定时任务状态
	 * 
	 * @param job
	 */
	void changeStatus(SysJob job);

	/**
	 * 删除调度任务，并停止定时任务
	 * 
	 * @param jobId
	 */
	void deleteJobById(Long jobId);

	/**
	 * 删除多个定时任务
	 * 
	 * @param ids
	 */
	void deleteByIds(Long[] jobIds);

	/**
	 * 删除定时任务
	 * 
	 * @param job
	 * @return
	 */
	int deleteJob(SysJob job);

	/**
	 * 回复定时任务
	 * 
	 * @param job
	 */
	void resumeJob(SysJob job);

	/**
	 * 暂停定时任务
	 * 
	 * @param job
	 */
	void pauseJob(SysJob job);

}
