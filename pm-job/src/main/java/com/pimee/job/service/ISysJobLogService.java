package com.pimee.job.service;

import com.github.pagehelper.PageInfo;
import com.pimee.common.service.IBaseService;
import com.pimee.job.model.SysJobLog;

import java.util.Map;

public interface ISysJobLogService extends IBaseService<SysJobLog> {
	/**
	 * 分页列表
	 * 
	 * @param params
	 * @return
	 */
	PageInfo<SysJobLog> pageList(Map<String, Object> params);

}
