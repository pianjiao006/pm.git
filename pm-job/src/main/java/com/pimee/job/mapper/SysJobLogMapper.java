package com.pimee.job.mapper;

import com.pimee.job.model.SysJobLog;
import tk.mybatis.mapper.common.Mapper;

public interface SysJobLogMapper extends Mapper<SysJobLog> {
}