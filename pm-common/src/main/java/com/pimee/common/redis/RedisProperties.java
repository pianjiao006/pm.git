package com.pimee.common.redis;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Component
@ConfigurationProperties(prefix = "spring.redis")
@Data
@EqualsAndHashCode(callSuper = false)
@Order(-1)
public class RedisProperties {

	public RedisProperties() {
		System.out.println("RedisProperties......");
	}
	private String host;
	private int port;
	private int database;
	private int timeout;
	private int maxIdle;
	private int minIdle;
	private int maxActive;
	private int maxWait;

}
