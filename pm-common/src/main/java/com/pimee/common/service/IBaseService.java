package com.pimee.common.service;

import java.util.List;

public interface IBaseService<T> {

	T selectByKey(Object key);

	int save(T entity);

	int saveNotNull(T entity);

	int deleteByKey(Object key);

	int updateAll(T entity);

	int updateNotNull(T entity);

	List<T> selectByExample(Object example);

	int countByExample(Object example);

	T selectOne(T entity);

	T selectOneByExample(Object example);

	List<T> listAll();

	/**
	 * 删除符合某些条件的数据
	 * 
	 * @param example
	 * @return
	 */
	int deleteBy(Object example);

	/**
	 * 根据条件更新数据
	 * @param entity
	 * @param example
	 * @return
	 */
	int updateNotNullByExample(T entity, Object example);

}
