package com.pimee.common.exception;

/**
 * 用户密码不正确或不符合规范异常类
 * @author Bruce Shaw
 * 2020年4月13日 下午5:12:42
 */
public class UserPasswordNotMatchException extends BusinessException {
	private static final long serialVersionUID = 1L;

	public UserPasswordNotMatchException() {
		super("user.password.not.match");
	}

	public UserPasswordNotMatchException(Integer code, String errMsg) {
		super(code, errMsg);
	}
}
