package com.pimee.common.exception;

/**
 * 
 * @author Bruce Shaw 2020年1月16日 下午7:50:34
 *
 */
public class BusinessException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int code;
	private String msg;
	private Object data;

	public BusinessException(String msg) {
		this.msg = msg;
	}

	public BusinessException(int code, String msg) {
		super(msg);
		this.code = code;
		this.msg = msg;
		this.data = null;
	}

	public BusinessException(int code, String msg, Object data) {
		super(msg);
		this.code = code;
		this.msg = msg;
		this.data = data;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}
}
