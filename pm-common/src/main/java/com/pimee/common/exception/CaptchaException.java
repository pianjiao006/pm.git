package com.pimee.common.exception;

import com.pimee.common.core.support.HttpCode;

public class CaptchaException extends BusinessException {
	private static final long serialVersionUID = 1L;

	public CaptchaException(String msg) {
		super(msg);
		this.setCode(HttpCode.INTERNAL_SERVER_ERROR.value());
	}
}
