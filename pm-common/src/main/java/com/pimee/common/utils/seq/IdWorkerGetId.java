package com.pimee.common.utils.seq;

import tk.mybatis.mapper.genid.GenId;

public class IdWorkerGetId implements GenId<Long> {

	@Override
	public Long genId(String table, String column) {
		return IdWorker.getId();
	}
	
}
