package com.pimee.common.utils.net;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSONObject;
import com.pimee.common.utils.Md5Util;
import com.pimee.common.utils.RandomUtil;
import com.pimee.common.utils.net.HttpUtil.Params;
import com.pimee.common.utils.net.HttpUtil.ReqParam;

/**
 * 将入参生成摘要
 * 
 * @author Bruce Shaw 2020年4月13日 下午3:09:04
 */
public class AuthUtil {
	// 这里写键值对，程序中使用ConfigProperties.props.get("")来获得值
	// 接口调用,约定的一个key值,用于md5加密
	private static final String HMAC_KEY = "sign";
	private static String clientId = "XXXX";
	private static String secret = "3XXXX";
	private static Logger logger = LoggerFactory.getLogger(AuthUtil.class);

	public static Params getParamListWithDigest(Params params) {
		return getParamListWithDigestNew(params);
	}

	public static Params getParamListWithDigestNew(Params params) {
		logger.info("计算摘要:{}", JSONObject.toJSONString(params));

		List<ReqParam> paramList = null;
		if (params != null) {
			paramList = params.getList();
		} else {
			paramList = new ArrayList<>();
		}

		long timestamp = System.currentTimeMillis();
		String nonce = RandomUtil.getRandomChar(6);
		String hmac = Md5Util.md5(clientId + secret + timestamp + nonce);

		// 加入摘要字段
		List<ReqParam> paramList2 = new ArrayList<>(paramList);
		paramList2.add(new ReqParam("clientId", clientId));
		paramList2.add(new ReqParam("timestamp", timestamp));
		paramList2.add(new ReqParam("nonce", nonce));
		paramList2.add(new ReqParam(HMAC_KEY, hmac));
		return Params.create().add(paramList2);
	}

	/**
	 * 获取发送json的加密信息
	 * 
	 * @param paramStr
	 * @return
	 */
	public static String getDigestJson(String paramStr) {
		logger.info("===> getDigestJson计算摘要:{}", paramStr);
		JSONObject param = JSONObject.parseObject(paramStr);

		long timestamp = System.currentTimeMillis();
		String nonce = RandomUtil.getRandomChar(6);
		String hmac = Md5Util.md5(clientId + secret + timestamp + nonce);

		param.put("clientId", clientId);
		param.put("timestamp", timestamp);
		param.put("nonce", nonce);
		param.put(HMAC_KEY, hmac);
		return JSONObject.toJSONString(param);
	}

	public static String getDigestJsonTest(String paramStr, String secret) {
		logger.info("===> getDigestJson计算摘要:{}", paramStr);
		JSONObject param = JSONObject.parseObject(paramStr);

		long timestamp = System.currentTimeMillis();
		String nonce = RandomUtil.getRandomChar(6);
		String clientId = param.getString("clientId");
		String hmac = Md5Util.md5(clientId + secret + timestamp + nonce);

		param.put("clientId", clientId);
		param.put("timestamp", timestamp);
		param.put("nonce", nonce);
		param.put(HMAC_KEY, hmac);
		return JSONObject.toJSONString(param);
	}

	/**
	 * 获取发送json的加密信息
	 * 
	 * @param paramStr
	 * @param secret
	 * @return
	 */
	public static String getDigestJsonStr(String paramStr, String clientId, String secret) {
		logger.info("===> getDigestJson计算摘要:{}", paramStr);
		JSONObject param = JSONObject.parseObject(paramStr);

		long timestamp = System.currentTimeMillis();
		String nonce = RandomUtil.getRandomChar(6);
		String sign = Md5Util.md5(clientId + secret + timestamp + nonce);

		param.put("clientId", clientId);
		param.put("timestamp", timestamp);
		param.put("nonce", nonce);
		param.put(HMAC_KEY, sign);
		return JSONObject.toJSONString(param);
	}

	public static void main(String[] args) {
		Params params = Params.create().add("", 115).add("", "说的是");
		String s = JSONObject.toJSONString(getParamListWithDigest(params));
		System.out.println(s);
	}
}
