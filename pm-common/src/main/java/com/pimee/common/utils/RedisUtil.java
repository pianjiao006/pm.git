package com.pimee.common.utils;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.connection.DataType;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.JdkSerializationRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.stereotype.Component;

/**
 * Redis通用操作类<br>
 * 
 * 使用本类的好处: 方法的每个细节都经过测试, 使用上可以掌控细节, 排除顾虑.<br>
 * 
 * 【本类已经通过了Redis 3.0.5版本的测试, 其中list_setList(rpush)方法在 2.4.5 版本报错,在3.0.5运行良好】<br>
 * 
 * 
 * 
 * 可以不使用泛型进行操作：
 * 
 * private RedisUtil baseRedisUtil; <br>
 * 
 *           也可以使用泛型：<br>
 * private RedisUtil<T> baseRedisUtilNmUser;<br>
 * 
 * @author xiaoxb
 *
 * @param <V>
 */
@SuppressWarnings(value = { "unchecked", "rawtypes" })
@Component
public class RedisUtil<V> {
	private static Logger logger = LoggerFactory.getLogger(RedisUtil.class);
	@Resource
	protected RedisTemplate<String, V> redisTemplate;

	private Map<String, RedisSerializer> serMap = new ConcurrentHashMap<>();
	{
		serMap.put("string", new StringRedisSerializer());
		serMap.put("jdk", new JdkSerializationRedisSerializer());
		serMap.put("json2", new Jackson2JsonRedisSerializer(Object.class));
	}
	
	@PostConstruct
	public synchronized void init() {
		redisTemplate.setKeySerializer(serMap.get("string"));
		redisTemplate.setValueSerializer(serMap.get("jdk"));
		redisTemplate.setHashKeySerializer(serMap.get("string"));
		redisTemplate.setHashValueSerializer(serMap.get("json2"));
		
		this.println("KeySerializer init: " + redisTemplate.getKeySerializer());
		this.println("ValueSerializer init: " + redisTemplate.getValueSerializer());
		this.println("HashKeySerializer init: " + redisTemplate.getHashKeySerializer());
		this.println("HashValueSerializer init: " + redisTemplate.getHashValueSerializer());
	}
	
	private void println(String msg) {
		//System.out.println(msg);
		logger.info(msg);
	}
	
	/**
	 * ##########################################
	 * #										#
	 * #		   string类型相关操作				#
	 * #										#
	 * ##########################################
	 */
	/**
	 * 覆盖set，可以指定过期时间(原key若无过期时间，覆盖设置后带过期时间, 原key带过期时间, 设置后时间重新开始计算倒计时)
	 * 
	 * @param key
	 * @param value
	 * @param seconds
	 * @author xiaoxb
	 */
	public void string_setExpireInSec(String key, V value, long seconds) {
		redisTemplate.opsForValue().set(key, value, seconds, TimeUnit.SECONDS);
	}
	
	public void string_setExpireInMin(String key, V value, long minutes) {
		redisTemplate.opsForValue().set(key, value, minutes, TimeUnit.MINUTES);
	}

	/**
	 * 不管key是否存在，都能set进去，可能会覆盖.不会过期
	 * 
	 * @param key
	 * @param value
	 * @author xiaoxb
	 */
	public void string_setPermanent(String key, V value) {
		redisTemplate.opsForValue().set(key, value);
	}

	/**
	 * 如果key存在，则不set，反之set, 返回Boolean表示是否set了进去
	 * 
	 * @param key
	 * @param value
	 * @return
	 * @author xiaoxb
	 */
	public boolean string_setIfAbsent(String key, V value) {
		return redisTemplate.opsForValue().setIfAbsent(key, value);
	}

	/**
	 * 用key获取值. ★★注意其他类型用此方法会抛异常,其他类型例如list,hash...
	 * 
	 * @param key
	 * @return
	 * @author xiaoxb
	 */
	public V string_get(String key) {
		return redisTemplate.opsForValue().get(key);
	}

	
	
	/**
	 * ##########################################
	 * #										#
	 * #		   原子增减类型相关操作				#
	 * #										#
	 * ##########################################
	 */
	
	/**
	 * 原子自增，统计数字用(请不要在程序里先get,加1，最后set进去，这样会有差错，不是原子的操作)<br>
	 * 
	 * 第一次incr一个不存在的key，值会初始化为1<br>
	 * 
	 * 可能会抛出异常，例如已经存在的key值是非数字可增加的类型，或者数字增加后会超过范围 ★注意java_String类型不能增减,由于使用的是JdkSerializationRedisSerializer,不要用这个进行增减
	 * 
	 * @param key
	 * @return 返回操作后的值
	 * @author xiaoxb
	 */
	public Long string_incrOne(String key) {
		return redisTemplate.opsForValue().increment(key, 1);
	}

	/**
	 * 原子自减，统计数字用<br>
	 * 
	 * 可能会抛出异常，例如已经存在的key值是非数字可增加的类型，或者数字增加后会超过范围★注意java_String类型不能增减,由于使用的是JdkSerializationRedisSerializer,不要用这个进行增减
	 * 
	 * @param key
	 * @return 返回操作后的值
	 * @author xiaoxb
	 */
	public Long string_decrOne(String key) {
		return redisTemplate.opsForValue().increment(key, -1);
	}

	/**
	 * 原子增加或减少, 增加或减去delta, 减去传负数<br>
	 * 
	 * 可能会抛出异常，例如已经存在的key值是非数字可增加的类型，或者数字增加后会超过范围 ★注意java_String类型不能增减,由于使用的是JdkSerializationRedisSerializer,不要用这个进行增减
	 * 
	 * @param key
	 * @param delta
	 * @return
	 * @author xiaoxb
	 */
	public Long string_increment(String key, long delta) {
		return redisTemplate.opsForValue().increment(key, delta);
	}

	
	/**
	 * ##########################################
	 * #										#
	 * #		         删除key相关操作				#
	 * #										#
	 * ##########################################
	 */
	/**
	 * 删除key值的缓存，删除不存在不抛异常.  可以删除各种类型string/list/hash/set/zset...
	 * 
	 * @param key
	 * @return
	 * @author xiaoxb
	 */
	public void delete(String key) {
		redisTemplate.delete(key);
	}

	/**
	 * 删除key值的集合, 删除不存在不抛出异常.  可以删除各种类型string/list/hash/set/zset...
	 * 
	 * @param keys
	 * @author xiaoxb
	 */
	public void delete(Collection<String> keys) {
		redisTemplate.delete(keys);
	}

	
	/**
	 * ##########################################
	 * #										#
	 * #		   	hash类型相关操作				#
	 * #			★★map是有序的!!				#
	 * #										#
	 * #										#
	 * ##########################################
	 */
	/**
	 * 设置到key值,key不存在则创建,存在则用已有,field为Map的键,该Map存在该field则覆盖,不存在则插入
	 * @param key
	 * @param field
	 * @param value
	 * @author xiaoxb
	 */
	public void hash_putField(String key, String field, V value) {
		getHashOps().put(key, field, value);
	}
	
	/**
	 * 如果key不存在则创建该key,如果key存在,但field不存在,也会创建field<br>  ★注意java_String类型不能增减,java_Integer,java_Long可以
	 * @param key
	 * @param field
	 * @param delta
	 * @return 返回的值是操作过后的该field的值
	 * @author xiaoxb
	 */
	public long hash_increment(String key, String field, long delta) {
		return getHashOps().increment(key, field, delta);
	}
	
	/**
	 * key不存在则创建新的,存在则用已有的,不是简单的把原来的map删掉,而是融合,遍历m,逐个put进原来的map里(存在则替换,不存在则插入)
	 * @param key
	 * @param map
	 * @author xiaoxb
	 */
	public void hash_putMap(String key, Map<String, V> map) {
		getHashOps().putAll(key, map);
	}

	/**
	 * 获取某个key下的某个field,当key不存在,或者field不存在,都返回null
	 * @param key
	 * @param field
	 * @return
	 * @author xiaoxb
	 */
	public V hash_getField(String key, String field) {
		return getHashOps().get(key, field);
	}
	
	/**
	 * 获取某个map,key不存在返回空的map   ★★注意不是null,是有序的! <br>
	 * 
	 * @param key
	 * @return
	 * @author xiaoxb
	 */
	public Map<String, V> hash_getMap(String key) {
		// 返回的其实是class java.util.LinkedHashMap
		return getHashOps().entries(key);// 不存在的key返回空map
	}

	/**
	 * 返回的是java.util.LinkedHashSet,是有序的key集合<br>
	 * key不存在返回空set(非null)
	 * 
	 * @param key
	 * @return
	 * @author xiaoxb
	 */
	public Set<String> hash_getMapKey(String key) {
		return getHashOps().keys(key);
	}
	
	/**
	 * 返回的是java.util.ArrayList,是有序的<br>
	 * 获取某个map的value,key不存在返回空list(非null)<br>
	 * @param key
	 * @return
	 * @author xiaoxb
	 */
	public List<V> hash_getMapValue(String key) {
		return getHashOps().values(key);
	}
	
	/**
	 * ★★key值不存在,返回的是[null,null](假如hashKeys有两个元素),
	 * 
	 * hashKeys中不存在的,返回null
	 * 
	 * @param key
	 * @param hashKeys
	 * @return
	 * @author xiaoxb
	 */
	public List<V> hash_getMapValue(String key, Collection<String> hashKeys) {
		return getHashOps().multiGet(key, hashKeys);
	}
	/**
	 * key不存在则返回0,key存在,若类型不是hash,则抛异常,是hash,则计算size
	 * 
	 * @param key
	 * @return
	 * @author xiaoxb
	 */
	public long hash_size(String key) {
		return redisTemplate.opsForHash().size(key);
	}

	public void hash_delete(String key, String... hashKeys) {
//		getHashOps().delete(key, hashKeys);
	}

	public void hash_delete(String key) {
		getHashOps().getOperations().delete(key);
	}


	/**
	 * ##########################################
	 * #										#
	 * #		   list类型相关操作				#
	 * #										#
	 * ##########################################
	 */
	/**
	 * key不存在则创建,key存在则追加values到该list的末尾(相当于rpush)
	 * 
	 * @param key
	 * @param values   ★★values是null或者空集合的时候会抛异常
	 * @author xiaoxb
	 */
	public void list_tailPushList(String key, Collection<V> values) {
		redisTemplate.opsForList().rightPushAll(key, values);
	}
	
	/**
	 * 插到头部. <br>
	 * 
	 * key不存在则创建list并插入值,存在则追加,追加的元素value允许重复<br>
	 * 
	 * Redis Destop中查看到是在 头部 插入元素(顶部)
	 * 
	 * @param key
	 * @param value
	 * @author xiaoxb
	 */
	public void list_headPush(String key, V value) {
		redisTemplate.opsForList().leftPush(key, value);
	}
	
	/**
	 * 插到尾巴. <br>
	 * 
	 * key不存在则创建list并插入值,存在则追加,追加的元素value允许重复<br>
	 * 
	 * Redis Destop中查看到是在 尾部 插入元素(底部)
	 * 
	 * @param key
	 * @param value
	 * @author xiaoxb
	 */
	public void list_tailPush(String key, V value) {
		redisTemplate.opsForList().rightPush(key, value);
	}
	
	/**
	 * key不存在返回空集合(不是null), 不是list会抛异常, 注意包括了开始也包括结束<br>
	 * 
	 * 例子:<br>
	 * 1、[0,0]取到第一个<br>
	 * 2、[1,1]取到第二个<br>
	 * 3、[0,1]取第一个和第二个<br>
	 * 4、[-1,1]取到空集合(不是null),倒数第一个到第二个之间没有元素,不是环状<br>
	 * 5、[0,-1]取所有(第一个到倒数第一个)<br>
	 * 6、[0,-2]取所有(第一个到倒数第二个)[1,-1]去第二个到尽头<br>
	 * 
	 * 详细的解释跟踪进源码查看注释 redis.clients.jedis.BinaryJedis
	 * 
	 * @param key
	 * @param start
	 * @param end
	 * @return
	 * @author xiaoxb
	 */
	public List<V> list_range(String key, long start, long end) {
		return redisTemplate.opsForList().range(key, start, end);
	}
	
	/**
	 * key不存在返回空集合(不是null), 不是list会抛异常
	 * 
	 * @param key
	 * @return
	 * @author xiaoxb
	 */
	public List<V> list_getAll(String key) {
		return redisTemplate.opsForList().range(key, 0, -1);
	}
	
	/**
	 * key不存在则返回0,key存在,若类型不是list,则抛异常,是list,则计算size
	 * @param key
	 * @return
	 * @author xiaoxb
	 */
	public long list_size(String key) {
		return redisTemplate.opsForList().size(key);
	}
	
	
	/**
	 * ##########################################
	 * #										#
	 * #		   set类型相关操作				#
	 * #										#
	 * ##########################################
	 */
	
	/**
	 * key不存在返回0,key存在类型不是set抛异常
	 * @param key
	 * @return
	 * @author xiaoxb
	 */
	public long set_size(String key) {
		return redisTemplate.opsForSet().size(key);
	}
	
	/**
	 * 无需.key不存在则创建,存在则合并set,返回插入的个数
	 * @param key
	 * @param value
	 * @return
	 * @author xiaoxb
	 */
	public long set_add(String key, V value) {
		return redisTemplate.opsForSet().add(key, value);
	}
	
	/**
	 * 无序.key不存在则创建,存在则合并set
	 * @param key
	 * @param values
	 * @return
	 * @author xiaoxb
	 */
	public long set_add(String key, V[] values) {
		return redisTemplate.opsForSet().add(key, values);
	}
	
	/**
	 * key不存在返回false,key存在,则判断o是否在set里. ★★若key的类型是string,抛错,list类型不抛错返回false,hash类型抛错,zset类型抛出异常
	 * @param key
	 * @param o
	 * @return
	 * @author xiaoxb
	 */
	public boolean set_isMember(String key, Object o) {
		return redisTemplate.opsForSet().isMember(key, o);
	}
	
	
	/**
	 * key不存在返回空的set(非null), key类型不对抛异常(list返回空集合[非null])
	 * @param key
	 * @return
	 * @author xiaoxb
	 */
	public Set<V> set_getAll(String key) {
		return redisTemplate.opsForSet().members(key);
	}
	
	/**
	 * 该接口只是合并结果后返回,并不会在key里追加otherKey改变key<br>
	 * 
	 * key和otherKey要保持是同一个类型<br>
	 * key不存在,otherKey不存在,返回空set(非null)<br>
	 * key不存在,otherKey存在,返回otherKey的set<br>
	 * key存在,但otherKey不存在,返回key的set,但是key的set没有任何改变<br>
	 * key存在,otherKey也存在,返回合并后的结果,key和otherKey不会有任何改变
	 * 
	 * @param key
	 * @param otherKey
	 * @return
	 * @author xiaoxb
	 */
	public Set<V> set_union(String key, String otherKey) {
		return redisTemplate.opsForSet().union(key, otherKey);
	}
	
	/**
	 * 求交集
	 * @param key
	 * @param otherKey
	 * @return
	 * @author xiaoxb
	 */
	public Set<V> set_intersect(String key, String otherKey) {
		return redisTemplate.opsForSet().intersect(key, otherKey);
	}
	
	
	/**
	 * ##########################################
	 * #										#
	 * #		   zset类型相关操作				#
	 * #										#
	 * ##########################################
	 */
	/**
	 * zset不允许重复. 插入了返回true,否则false.score允许相同.排序规则是按照score从小到大,score相同的时候插入时间在后的排在后面
	 * 
	 * @param key
	 * @param value
	 * @param score
	 * @return
	 * @author xiaoxb
	 */
	public boolean zset_add(String key, V value, double score) {
		return redisTemplate.opsForZSet().add(key, value, score);
	}
	
	
	/**
	 * 跟list_range的特性完全一样, 不是zset会抛异常(除list不会,set会)
	 * @param key
	 * @param start
	 * @param end
	 * @return
	 * @author xiaoxb
	 */
	public Set<V> zset_range(String key, long start, long end) {
		return redisTemplate.opsForZSet().range(key, start, end);
	}
	
	/**
	 * key不存在返回空集合(不是null), 不是zset会抛异常(除list不会,set会)
	 * @param key
	 * @return
	 * @author xiaoxb
	 */
	public Set<V> zset_getAll(String key) {
		return redisTemplate.opsForZSet().range(key, 0, -1);
	}
	
	
	/**
	 * ##########################################
	 * #										#
	 * #		           超时相关操作					#
	 * #										#
	 * ##########################################
	 */
	/**
	 * 让某个key过时,返回true表示设置成功(key存在),返回false表示(一般是key不存在, 可能设置) <br>
	 * 
	 *  可以重复设置,返回true,重复设置后时间重新计算,相当于续命
	 * 
	 * @param key
	 * @param seconds
	 * @return
	 * @author xiaoxb
	 */
	public boolean expireInSeconds(String key, long seconds) {
		return redisTemplate.expire(key, seconds, TimeUnit.SECONDS);
	}
	
	/**
	 * 让某个key过时,返回true表示设置成功(key存在),返回false表示(一般是key不存在) <br>
	 * 
	 * 可以重复设置,返回true,重复设置后时间重新计算,相当于续命
	 * 
	 * @param key
	 * @param minutes
	 * @return
	 * @author xiaoxb
	 */
	public boolean expireInMinutes(String key, long minutes) {
		return redisTemplate.expire(key, minutes, TimeUnit.MINUTES);
	}
	
	/**
	 * 让某个key过时,返回true表示设置成功(key存在),返回false表示(一般是key不存在) <br>
	 * 
	 * 可以重复设置,返回true,重复设置后时间重新计算,相当于续命
	 * 
	 * @param key
	 * @param time
	 * @param unit
	 * @return
	 * @author xiaoxb
	 */
	public boolean expire(String key, long time, TimeUnit unit) {
		return redisTemplate.expire(key, time, unit);
	}
	
	
	/**
	 * 判断key值的类型,如果key值的键不存在,返回枚举NONE,其他会返回枚举STRING/LIST/HASH/SET/ZSET
	 * @param key
	 * @return
	 * @author xiaoxb
	 */
	public DataType type(String key) {
		return redisTemplate.type(key);
	}
	
	/**
	 * 检测某个key是否存在,存在则true.
	 * @param key
	 * @return
	 * @author xiaoxb
	 */
	public boolean exists(final String key) {
		return redisTemplate.execute(new RedisCallback<Boolean>() {
			@Override
			public Boolean doInRedis(RedisConnection connection) throws DataAccessException {
				RedisSerializer keySerializer = redisTemplate.getKeySerializer();
				byte[] k = keySerializer.serialize(key);
				return connection.exists(k);
			}
		});
	}
	
	/**
	 * 检测某个map的field是否存在,不是map的时候抛异常
	 * 
	 * @param key
	 * @param field
	 * @return
	 * @author xiaoxb
	 */
	public boolean existsField(final String key, final String field) {
		return redisTemplate.execute(new RedisCallback<Boolean>() {
			@Override
			public Boolean doInRedis(RedisConnection connection) throws DataAccessException {
				RedisSerializer keySerializer = redisTemplate.getKeySerializer();
				byte[] k = keySerializer.serialize(key);
				byte[] f = keySerializer.serialize(field);
				return connection.hExists(k, f);
			}
		});
	}
	/**
	 * 获取所有的keys
	 * @param pattern
	 * @return
	 */
	public Set keys(String pattern){
		return redisTemplate.keys(pattern);
	}
	
	/**
	 * 获得模版(如果这些常用方法还不够用,用模版)
	 * 
	 * @return
	 * @author xiaoxb
	 */
	public RedisTemplate<String, V> getTpl() {
		return redisTemplate;
	}
	
	private HashOperations<String, String, V> getHashOps() {
		return redisTemplate.opsForHash();
	}
}
