package com.pimee.common.utils;

/**
 * 处理并记录日志文件
 * @author Bruce Shaw
 * 2020年4月13日 下午4:33:27
 */
public class LogUtils
{
    public static String getBlock(Object msg)
    {
        if (msg == null)
        {
            msg = "";
        }
        return "[" + msg.toString() + "]";
    }
}
