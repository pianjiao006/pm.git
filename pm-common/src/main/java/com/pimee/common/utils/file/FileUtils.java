package com.pimee.common.utils.file;

import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.net.URLEncoder;

/**
 * 文件处理工具类
 * 
 * @author Bruce Shaw 2020年2月4日 下午11:24:44
 *
 */
public class FileUtils {
	public static String FILENAME_PATTERN = "[a-zA-Z0-9_\\-\\|\\.\\u4e00-\\u9fa5]+";

	/**
	 * 输出指定文件的byte数组
	 * 
	 * @param filePath
	 *            文件路径
	 * @param os
	 *            输出流
	 * @return
	 */
	public static void writeBytes(String filePath, OutputStream os) throws IOException {
		FileInputStream fis = null;
		try {
			File file = new File(filePath);
			if (!file.exists()) {
				throw new FileNotFoundException(filePath);
			}
			fis = new FileInputStream(file);
			byte[] b = new byte[1024];
			int length;
			while ((length = fis.read(b)) > 0) {
				os.write(b, 0, length);
			}
		} catch (IOException e) {
			throw e;
		} finally {
			if (os != null) {
				try {
					os.close();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
			if (fis != null) {
				try {
					fis.close();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		}
	}

	/**
	 * 删除文件
	 * 
	 * @param filePath
	 *            文件
	 * @return
	 */
	public static boolean deleteFile(String filePath) {
		boolean flag = false;
		File file = new File(filePath);
		// 路径为文件且不为空则进行删除
		if (file.isFile() && file.exists()) {
			file.delete();
			flag = true;
		}
		return flag;
	}

	/**
	 * 文件名称验证
	 * 
	 * @param filename
	 *            文件名称
	 * @return true 正常 false 非法
	 */
	public static boolean isValidFilename(String filename) {
		return filename.matches(FILENAME_PATTERN);
	}

	/**
	 * 下载文件名重新编码
	 * 
	 * @param request
	 *            请求对象
	 * @param fileName
	 *            文件名
	 * @return 编码后的文件名
	 */
	public static String setFileDownloadHeader(HttpServletRequest request, String fileName)
			throws UnsupportedEncodingException {
		final String agent = request.getHeader("USER-AGENT");
		String filename = fileName;
		if (agent.contains("MSIE")) {
			// IE浏览器
			filename = URLEncoder.encode(filename, "utf-8");
			filename = filename.replace("+", " ");
		} else if (agent.contains("Firefox")) {
			// 火狐浏览器
			filename = new String(fileName.getBytes(), "ISO8859-1");
		} else if (agent.contains("Chrome")) {
			// google浏览器
			filename = URLEncoder.encode(filename, "utf-8");
		} else {
			// 其它浏览器
			filename = URLEncoder.encode(filename, "utf-8");
		}
		return filename;
	}
	
	/**
	 * 保证路径的目录都创建好
	 * @param path
	 * @return
	 * @author xiaoxb
	 */
	public static String ensureFileDir(String path) {
		try {
			File directoryFile = new File(path);
			if (!directoryFile.exists()) {
				directoryFile.mkdirs(); // 有s
			}
		} catch (Exception e) {
		}
		return path;
	}
	/**
	 * 上传文件到本地
	 * @param file
	 * @param uploadDirConstStr
	 * @param basePathDefault
	 * @param hostDefault
	 * @param fileName
	 * @return
	 */
	public static String uploadFile(MultipartFile file, String uploadDirConstStr , String basePathDefault, String hostDefault, String fileName) {
		String relativeDir = uploadDirConstStr;
		String relativePath =  relativeDir + fileName;
		String basePath =  basePathDefault;
		if(file != null && !file.isEmpty()) {
			File dest = new File(basePath + relativePath);
//            File dest = new File("F:\\code\\images" +basePath + relativePath);
			ensureFileDir(basePath + relativeDir);//确保存放文件的目录是否存在
			try {
				file.transferTo(dest);
			} catch (IllegalStateException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		String logoPath = hostDefault + relativePath;
		return logoPath;
	}
}
